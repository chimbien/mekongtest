<?php
/*
  Template Name: Template Home
 */
?>
<?php get_header(); ?>
<?php
if (wpmd_is_phone()) {
  include_once 'mobile/home.php';
  get_footer();
  return;
}
?>
<style>
    .footer{
        display:none;
    }

    /*
    .header {
        background: none repeat scroll 0 0 #FAFAFA;
        float: left;
        height: 85px;
        margin-top: -0px;
        overflow: hidden;
        width: 100%;
    }
    */
    #thumb-tray {
        bottom: 0px !important;
        height: 167px;
        left: 0;
        overflow: hidden;
        padding: 20px 30px 20px 20px;
        position: fixed;
        text-align: center;
        width: 100%;
        z-index: 3;
    }

</style>
<?php
$defaults = array(
    'post_parent' => $post->ID,
    'orderby' => 'menu_order',
    'order' => 'ASC',
    'post_type' => 'page',
    'numberposts' => -1,
    'post_status' => 'publish'
);
$page_parent = get_children($defaults);
$i = 1;
?>

<div class="hide-for-small">
    <!-- Slide Home -->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/lib/slideshow/css/supersized.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/lib/slideshow/theme/supersized.shutter.css" type="text/css" media="screen" />

    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/lib/slideshow/js/jquery.easing.min.js"></script>

    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/lib/slideshow/js/supersized.3.2.7.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/lib/slideshow/theme/supersized.shutter.min.js"></script>
    <!-- End Slide Home -->
    <script type="text/javascript">
                        
        jQuery(function($){
		
			$(".header").css("max-width","100%");
            $(".line").css("max-width","100%");                   
            $.supersized({
                                
                // Functionality
                slideshow  :   1,			// Slideshow on/off
                autoplay	:	0,			// Slideshow starts playing automatically
                start_slide   :   1,			// Start slide (0 is random)
                stop_loop	:	0,			// Pauses slideshow on last slide
                random					: 	0,			// Randomize slide order (Ignores start slide)
                slide_interval          :   3000,		// Length between transitions
                transition              :   6, 			// 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
                transition_speed		:	1000,		// Speed of transition
                new_window				:	0,			// Image links open in new window/tab
                pause_hover             :   1,			// Pause slideshow on hover
                keyboard_nav            :   1,			// Keyboard navigation on/off
                performance				:	3,			// 0-Normal, 1-Hybrid speed/quality, 2-Optimizes image quality, 3-Optimizes transition speed // (Only works for Firefox/IE, not Webkit)
                image_protect			:	1,			// Disables image dragging and right click with Javascript
                                                                                                                           
                // Size & Position						   
                min_width		        :   0,			// Min width allowed (in pixels)
                min_height		        :   0,			// Min height allowed (in pixels)
                vertical_center         :   0,			// Vertically center background
                horizontal_center       :   1,			// Horizontally center background
                fit_always				:	0,			// Image will never exceed browser width or height (Ignores min. dimensions)
                fit_portrait         	:   1,			// Portrait images will not exceed browser height
                fit_landscape			:   1,			// Landscape images will not exceed browser width
                                                                                                                           
                // Components							
                slide_links				:	'blank',	// Individual links for each slide (Options: false, 'num', 'name', 'blank')
                thumb_links				:	1,			// Individual thumb links for each slide
                thumbnail_navigation    :   0,			// Thumbnail navigation
                slides 					:  	[			// Slideshow Images
                <?php foreach ($page_parent as $index => $re): ?>
                    <?php $content = apply_filters('the_content', $re->post_content); ?>
                    <?php 
					//$contentht = trim(preg_replace('/[\s]/', ' ', strip_tags($content)));
					$contentht = trim(str_replace('  ', ' ', strip_tags($content)));
                    //$contentht = strip_tags($content);
                    
                    ?>
                    <?php $image = wp_get_attachment_image_src(get_post_meta($re->ID, 'image', true),'full'); ?>    
                    {image : '<?php echo  $image[0];?>',
                        verticalAlign:'<?php echo get_post_meta($re->ID, 'vertical_align', true); ?>',
                        title : '<?php echo $re->post_title; ?>',
                        showContent : 1,
                        contentBgColor: '<?php  echo get_post_meta($re->ID, 'box_color', true); ?>',
                        contentTitle : '<?php echo $re->post_title; ?>',
                        contentHeadline: '<?php echo get_post_meta($re->ID, 'box_headline', true); ?>',
                        contentLink1:'<?php echo get_post_meta($re->ID, 'button_1_url', true); ?>',
                        contentLink2:'<?php echo get_post_meta($re->ID, 'button_2_url', true); ?>',
                        contentTitleLink1:'<?php echo get_post_meta($re->ID, 'button_1_title', true); ?>',
                        contentTitleLink2:'<?php echo get_post_meta($re->ID, 'button_2_title', true); ?>',
                        thumb:'<?php echo "<h3>".$re->post_title."</h3>"?><?php echo "<p>". $contentht."</p>"; ?>', url : "#"}<?php if($i < count($page_parent)):?>,<?php endif;?>
                    <?php $i++; ?>
                <?php endforeach; ?>                
            ],
                                                                                                
            // Theme Options			   
            progress_bar			:	0,			// Timer for each slide							
            mouse_scrub				:	0
                                        
        });
		
		
	
	
		$("#supersized li a").click(function(event){
			event.preventDefault();
		});
		$(".inner-content").each(function(){
			var color = $(this).children(".inner-content-bg").css("background-color");
			
			$(this).find("a").hover(function(){
					$(this).css("color",color);
				},
					function(){
						$(this).css("color","white");
						}
				);	
		});
    });
 		
		 if (/MSIE\s([\d.]+)/.test(navigator.userAgent)) {
			//Get the IE version.  This will be 6 for IE6, 7 for IE7, etc...
			version = new Number(RegExp.$1);
			if(version == 8){
				$(document).ready(function(){
            		$("#thumb-tray").animate({bottom:-20});
     			});
				 }
		 };
         $(document).ready(function() {
			
			//alert($(window).height());
			$("#swipe_area").swipe( {
				//Generic swipe handler for all directions
				swipeLeft:function(event, direction, distance, duration, fingerCount) {
					api.nextSlide();
				},
				swipeRight:function(event, direction, distance, duration, fingerCount) {
					api.prevSlide();
				},
				//Default is 75px, set to 0 for demo so any distance triggers swipe
				threshold:75,
				triggerOnTouchEnd: true
			});
			
			setTimeout(function(){
					
					$("#thumb-list li p").swipe( {
						//Generic swipe handler for all directions
						tap:function(event, target) {
						  $(this).trigger("click");
						},
						swipeLeft:function(event, direction, distance, duration, fingerCount) {
							//api.nextSlide();
							$("#thumb-forward").trigger("click");
						},
						swipeRight:function(event, direction, distance, duration, fingerCount) {
							//api.prevSlide();
							$("#thumb-back").trigger("click");
						},
						//Default is 75px, set to 0 for demo so any distance triggers swipe
						threshold:75
					});
					
				
			}, 3000);
			
			$(".inner-content").css("left","230px");
			
			
			
			
		
		});
		           
    </script>
    <div class="slide-home">
        <!--Thumbnail Navigation-->
        <!--<div id="prevthumb"></div>
        <div id="nextthumb"></div>-->

        <!--Arrow Navigation-->
        <a id="prevslide" class="load-item"></a>
        <a id="nextslide" class="load-item"></a>
<!--
        <div id="thumb-tray" class="load-item" style="display: block;">
            <div id="thumb-back-bg"></div>
            <div id="thumb-forward-bg"></div>
            <div id="thumb-back"></div>
            <div id="thumb-forward"></div>
        </div>
-->
        <div id="thumb-tray">
            <div id="thumb-back-bg"></div>
            <div id="thumb-forward-bg"></div>
            <div id="thumb-back"></div>
            <div id="thumb-forward"></div>
        </div>


        <!--Control Bar-->
        <div id="controls-wrapper">
            <div id="controls">

                <!--Navigation-->
                <ul id="slide-list"></ul>

            </div>
        </div>
    </div>
</div>
<div id="swipe_area" style="position:fixed; right:0px; width:55%; top:100px; height:55%; z-index:9000;"></div>
<?php get_footer(); ?>

<script>
$("li.item0").addClass("current-menu-item");
</script>