<?php
add_action( 'init', 'create_venue_tax' );
add_action( 'init', 'create_venue_post_types' );

function create_venue_tax() {
	register_taxonomy(
		'venue_cat',
		'venue',
		array(
			'label' => __( 'Phân loại Đại sảnh' ),
			'rewrite' => array( 'slug' => 'venue_cat' ),
			'hierarchical' => true,
		)
	);
}

function create_venue_post_types() {
	register_post_type( 'venue', 
		array(
			'labels' => array(
									'name' => __( 'Đại sảnh' ),
									'singular_name' => __( 'Đại sảnh' ),
									'add_new' => __( 'Thêm Đại sảnh mới' ),
									'add_new_item' => __( 'Thêm Đại sảnh mới' ),
									'edit' => __( 'Sửa' ),
									'edit_item' => __( 'Sửa' ),
									'new_item' => __( 'Thêm Đại sảnh mới' ),
									'view' => __( 'Xem' ),
									'view_item' => __( 'Xem' ),
									'search_items' => __( 'Tìm kiếm Đại sảnh' ),
									'not_found' => __( 'Không tìm thấy' ),
									'not_found_in_trash' => __( 'Không tìm thấy' ),
									'parent' => __( 'Đại sảnh cha' ),
								),
			'public' => true,
			'menu_position' => 20,
			'menu_icon' => get_stylesheet_directory_uri() . '/images/venue-icon.jpg',
			'supports' => array( 'title', 'editor', 'thumbnail' ),
			'taxonomies' => array('venue_cat'),
		)
	);
}

// Multilanguage
add_action('venue_cat_add_form',	 'qtrans_modifyTermFormFor');
add_action('venue_cat_edit_form',	 'qtrans_modifyTermFormFor');
?>