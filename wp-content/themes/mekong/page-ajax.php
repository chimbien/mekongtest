<?php
/*
  Template Name: Template Load News
 */
?>
<?php
    $offset = $_POST['offset'];
    $page_id = $_POST['post_id'];
    $start = $offset*5;
?>

<?php

$args = array(
    'cat' => $page_id,
    'posts_per_page' => 5,
    'post_status' => 'publish',
    'offset' => $start
);
query_posts($args);
if (have_posts()) : while (have_posts()) : the_post();
        get_template_part('content', get_post_format());
    endwhile;
endif;
wp_reset_query();
?>
</div>