<?php
/*
  Template Name: Template Donate
 */
?>
<?php
/*$to = get_option('admin_email');
$subject = get_option('mail_from_name');
//Send mail via AJAX.
if (isset($_POST['send_contact'])) {  

    $fullname = @$_POST['fullname'];
    $email = @$_POST['email'];
    $company = @$_POST['company'];
    $project = @$_POST['project'];
    $amount = @$_POST['amount'];
    $question = @$_POST['question'];
    //$message = $question;
    $message="<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" .
        "	<tr>" .
        "		<td>Fullname:</td>" .
        "		<td>" . $fullname . "</td>" .
        "	</tr>" .
        "	<tr>" .
        "		<td>Email:</td>" .
        "		<td>" . $email . "</td>" .
        "	</tr>" .
        "	<tr>" .
        "		<td>Company:</td>" .
        "		<td>" . $company. "</td>" .
        "	</tr>" .
        "	<tr>" .
        "		<td>Project:</td>" .
        "		<td>" . $project . "</td>" .
        "	</tr>" .
        "	<tr>" .
        "		<td>Amount:</td>" .
        "		<td>" . $amount . "</td>" .
        "	</tr>" .
        "	<tr>" .
        "		<td>Message:</td>" .
        "		<td>" . $question . "</td>" .
        "	</tr></table>";

    try {
        //Send mail
        $headers = "Content-type:text/html;charset=iso-8859-1" . "\r\n";
        $headers .= 'From: Mekong <no-reply@mekong.org.vn>' . "\r\n";
        wp_mail($to, $subject, $message, $headers);

        echo "OK";
    } catch (Exception $e) {
        echo 'heeeeeeee';
    }
    die();
}*/
?>
<?php get_header(); ?>
<?php
if (wpmd_is_phone()) {
    include_once 'mobile/page-donate.php';
    get_footer();
    return;
}
?>
<script>
    $("#menu-item-237").addClass("current-menu-item");
</script>
<?php while (have_posts()) : the_post(); ?>
    <div class="banner-donate">
        <img src="<?php echo get_field('page_banner'); ?>" alt="" />
        <div class="row">
            <div class="large-20 columns end">
                <div class="large-5 btn_donate">
                    <h1><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
    </div>
    <div class="donate-page">
        <div class="donate-section1">
            <div class="bg_top"></div>
            <div class="bg_repeat">
                <div class="row">
                    <div class="large-1 columns end ">&nbsp;</div>
                    <div class="large-18 small-20 columns end">
                        <div class="large-16 small-20 intro">
                            <?php the_content(); ?>
                        </div>
                        <!--<div class="large-16 small-20 intro" style="text-align: left;margin-top: 50px;">
                            <div class="large-10 small-20 columns end pR30">
                                <div class="inputform">
                                    <input type="text" value="" placeholder="Fullname*" name="fullname" id="fullname" />
                                </div>
                                <div class="inputform">
                                    <input type="text" value="" placeholder="Email*" name="email" id="email" />
                                </div>
                                <div class="inputform">
                                    <input type="text" value="" placeholder="Company" name="company" id="company" />
                                </div>
                                <div class="inputform">
                                    <?php
                                    $args = array(
                                        'child_of' => 182,
                                        'orderby' => 'menu_order',
                                        'order' => 'ASC',
                                        'post_type' => 'page',
                                        'post_status' => 'publish'
                                    );

                                    $project = get_pages($args);
                                    // print_r($project);
                                    ?>
                                    <select name="project" id="project" class="styled">
                                        <option value="0">Select Project</option>
                                        <?php foreach ($project as $p): ?>
                                            <option value="<?php echo $p->post_title; ?>"><?php echo $p->post_title; ?></option>
                                        <?php endforeach; ?>
                                        <option value="Other Project">Other Project</option>
                                    </select>
                                </div>
                            </div>
                            <div class="large-10 small-20 columns end pL30">

                                <div class="inputform">
                                    <input type="text" value="" placeholder="Amount" name="amount" id="amount" />
                                </div>
                                <div class="inputform">
                                    <textarea placeholder="Message*" name="message" id="message" style="height: 169px;"></textarea>
                                </div>
                                <div class="inputform">
                                    <p id="smsg"></p>
                                    <input type="submit" class="btn_submit" value="SEND US" />
                                </div>
                            </div>
                        </div>-->
                    </div>
                    <div class="large-1 columns end ">&nbsp;</div>
                </div>
            </div>
            <div class="bg_bottom"></div>
        </div>
		<br style="clear:both"/>
        <div class="link-involved ">
            <div class="row">
                <div class="large-5 columns end">&nbsp;</div>
                <div class="large-12 columns end btn-link-vol">
                    <?php if (ICL_LANGUAGE_CODE == 'vi'){
                    $language_text = '?lang=vi';
                    }?>
                    <?php
                    $volunteer = get_page(134);
                    ?>
                    <a href="<?php echo get_page_link(134).$language_text; ?>" class="large-6 small-10 columns end btn_vol1_small">
                        <?php echo $volunteer->post_title; ?>
                    </a>
                    <span class="large-3 small-10 columns end" style="line-height: 85px;font-size: 31px;color: #cdcdcd;"><?php if (ICL_LANGUAGE_CODE == 'vi'){ echo 'Hoặc';}else{ echo 'OR';} ?></span>
                    <a href="<?php echo get_page_link(); ?>" class="large-6 small-10 columns end btn_vol2_small">
                        <?php the_title(); ?>                
                    </a>
                </div>
                <div class="large-3 columns end">&nbsp;</div>
            </div>
        </div>

    </div>
<?php endwhile; // end of the loop.   ?>
<!--
<script type="text/javascript" language="javascript">		
    String.prototype.trim = function() {
        sString = this;
        sString = sString.replace(/^\s*/, "").replace(/\s*$/, "");
        return sString;
    };
    
    $(document).ready(function(){
        var $iName = $("#fullname");
        var $iCompany = $("#company");
        var $iEmail = $("#email");                   
        var $iQuestion = $("#message");
        var $iAmount = $("#amount");
        var $iProject = $("#project");
		
        var $iNameVal = $iName.val().trim();
        var $$iCompanyVal = $iCompany.val().trim();		
        var $iEmailVal = $iEmail.val().trim();                    					
        var $iQuestionVal = $iQuestion.val().trim();
		
        $("#smsg").hide();
		
        var errFlag = false;//noerr
        function validation(){
            var noterror = true;
            var $iNameVal = $iName.val().trim();
            var $iEmailVal = $iEmail.val().trim();
            var $iProjectVal = $iProject.val().trim();				
            var $iQuestionVal = $iQuestion.val().trim();
			
            if($iNameVal == ""){
                $iName.css('border-color','red');                
                noterror = false;
            }else{
                $iName.css('border-color','#EFEFEF');
            }
            if($iProjectVal == 0){
                $('.customSelect').css('border-color','red');                
                noterror = false;
            }else{
                $('.customSelect').css('border-color','#EFEFEF');
            }
            if($iEmailVal == ""){
                $iEmail.css('border-color','red');
                noterror = false;
            }else{
                if(!validate_email($iEmailVal)){
                    $iEmail.css('border-color','red');
                    noterror = false;
                }else{
                    $iEmail.css('border-color','#EFEFEF');
                }
            }
						
            if($iQuestionVal == ""){
                $iQuestion.css('border-color','red');
                noterror = false;
            }else{
                $iQuestion.css('border-color','#EFEFEF');;
            }
            return noterror;
        }
        function validate_email(val)
        {
            return /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(val);
        }
        
        $iProject.focus(function(){
            $('.customSelect').css('border-color','#EFEFEF');
        });
        
        $iName.focus(function(){
            $iName.css('border-color','#EFEFEF');
        });
		
        $iEmail.focus(function(){
            $iEmail.css('border-color','#EFEFEF');
        });
	
        $iQuestion.focus(function(){
            $iQuestion.css('border-color','#EFEFEF');
        });
		
        $(".btn_submit").click(function(event){
            event.preventDefault();
            if(validation()){
                var $iNameVal = $iName.val().trim();
                var $iCompanyVal = $iCompany.val().trim();		
                var $iEmailVal = $iEmail.val().trim();
                var $iAmountVal = $iAmount.val().trim();
                var $iProjectVal = $iProject.val(); 
                var $iQuestionVal = $iQuestion.val().trim();
				
                $("#smsg").show().html('<image src="<?php bloginfo('template_url'); ?>/images/lightbox-ico-loading.gif" />');
								
                $(".btn_submit").css('display','none');
				
                $.ajax({
                    url: '<?php echo get_page_link($post->ID) ?>',
                    data: {send_contact: "true",
                        fullname: $iNameVal,
                        email: $iEmailVal,
                        company: $iCompanyVal,
                        project: $iProjectVal,
                        amount: $iAmountVal,
                        question: $iQuestionVal
                    },
                    type: 'post'
                }).done(function(msg) {
                    if (msg == "OK") {
                        $("#smsg").html('Thank you very much for contacting us, We will reply you as soon as possible!');
                    } else {
                        $("#smsg").html('Your contact cannot be sent');
                    }
                });
            }
        });
    });	
</script>-->
<?php get_footer(); ?>
