<?php
/*
  Template Name: Template About Us
 */
?>
<?php get_header(); ?>
<?php
if (wpmd_is_phone()) {
  include_once 'mobile/page-about-us.php';
  get_footer();
  return;
}
?>
<?php while (have_posts()) : the_post(); ?>
    <div class="page-banner">
        <img src="<?php echo get_field('page_banner'); ?>" />
        <div class="row">
            <div class="large-9 small-20 page-decription">
                <?php echo get_field('introduction_page'); ?>
            </div>
        </div>

    </div>
    <div class="main-content">
        <div class="row">
            <div class="large-1 columns end">&nbsp;</div>
            <div class="show-for-small small-20 columns end">
                <?php echo get_field('introduction_page'); ?>
            </div>
            <div class="large-18 columns end">
                <div class="page-about-team">
                    <div class="large-20 columns end">
                        <div class="title-page-boxmenu">
                            <div class="title-page large-15 left"><?php the_title(); ?></div>
                            <div class="boxmenu large-4 right">
                                <?php the_title(); ?>
                            </div>
                            <div class="icon-boxmenu"></div>
                            <div class="menu-box large-4">
                                <?php
                                $args = array();
                                $args = array(
                                    'theme_location' => 'secondary',
                                    'container' => FALSE,
                                    'menu' => 'Menu about',
                                    'menu_class' => '',
                                    'echo' => true,
                                    'fallback_cb' => '',
                                    'depth' => 4,
                                    //'after' => '<li class="divider"></li>',                        
                                    'menu_id' => '');
                                wp_nav_menu($args);
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="page-about-content">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
            <div class="large-1 columns end">&nbsp;</div>
        </div>
    </div>
<?php endwhile; // end of the loop.  ?>
<script>
	$(document).ready(function(){
		$(".menu-box").show();
		setTimeout(function() {
    		$('.menu-box').slideToggle(3000);
		}, 5000);
		
		});
</script>
<?php get_footer(); ?>

