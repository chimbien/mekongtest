<div class="involve<?php echo get_post_meta($post->ID, 'position_box', true);?>">
    <div class="row">
        <div class="large-1 columns end ">&nbsp;</div>
        <div class="large-18 small-20 columns end">
            <div class="large-16 small-20 intro">
                <h2><?php the_title(); ?></h2>
                <?php the_content(); ?>
            </div>
        </div>
        <div class="large-1 columns end ">&nbsp;</div>
    </div>
</div>
