<?php
/*
  Template Name: Template Project
 */
?>
<?php get_header(); ?>
<?php
if (wpmd_is_phone()) {
    include_once 'mobile/page-project.php';
    get_footer();
    return;
}
?>
<?php while (have_posts()) : the_post(); ?>
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/lib/CircleHoverEffects/css/common.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/lib/CircleHoverEffects/css/style.css" />   
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/lib/ThumbnailGridExpandingPreview/css/default.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/lib/ThumbnailGridExpandingPreview/css/component.css" />
    <link rel="stylesheet"  media="screen" type="text/css" href="<?php bloginfo('template_url'); ?>/lib/fancybox2/jquery.fancybox.css?v=2.1.5.css" />
    <div class="page-banner ">
        <img src="<?php echo get_field('page_banner'); ?>" />
        <div class="row">
            <div class="large-20">
                <?php echo get_field('introduction_page'); ?>
            </div>
        </div>
    </div>
    <div class="main-page-project">
        <div class="row ">
            <div class="large-1 columns end">&nbsp;</div>
            <div class="large-18 columns end project">
                <div class="large-15 intro">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
        <?php
        $args = array(
            'post_parent' => $post->ID,
            'orderby' => 'menu_order',
            'order' => 'ASC',
            'post_type' => 'page',
            'numberposts' => -1,
            'post_status' => 'publish'
        );
        $page_parent = get_children($args);
        ?>
        <ul id="og-grid" class="og-grid ch-grid">
            <?php
            $j = 1;
            $i = 0;
            $h = 0;
            ?>
            <?php foreach ($page_parent as $index => $re): ?>
                <?php
                $temp = $i / 3;
                $temp2 = $j % 3;
                ?>
                <!--[if IE 8]>
                <?php if (($h % 3) == 0): ?>
                                                        <div class=" heightdefault row<?php echo (int) $temp; ?>">
                <?php endif; ?>
                        <![endif]-->
                <li class="<?php echo $h; ?>">
                    <a class="linkproject" href="<?php echo get_page_link($re->ID); ?>">
                        <div class="ch-item" style="background:url(<?php echo wp_get_attachment_url(get_post_meta($re->ID, 'thumbnail_image', true), 'full'); ?>) center center; background-size:100%;">
                            <div class="ch-info">
                                <?php echo wp_get_attachment_image(get_post_meta($re->ID, 'hover_image', true), 'full'); ?>

                            </div>
                        </div>
                        <h3 class="title"><?php echo $re->post_title; ?></h3>
                    </a>
                    <div class="data-content" style="display: none;">
                        <span class="og-closeie8"></span>
                        <div class="og-expander-innerie8">
                            <?php echo apply_filters('the_content', $re->post_content);
                            ?>
                            <?php if (get_post_meta($re->ID, 'link', true)): ?>
                                <br/>
                                <a class="readmore" href="<?php echo get_post_meta($re->ID, 'link', true); ?>" target="_blank">Read more</a>
                            <?php endif; ?>

                            <div class="box-project-gallery">
                                <?php if (get_post_meta($re->ID, 'gallery', true)): ?>
                                    <h5>IMAGES GALLERY</h5>

                                    <?php $content = apply_filters("the_content", get_post_meta($re->ID, 'gallery', true)); ?>
                                    <?php echo $content; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </li>
                <!--[if IE 8]>
                <?php if (((++$h % 3) == 0) || ($h == count($page_parent))): ?>                   
                                                </div>
                <?php endif; ?>
                <![endif]-->
                <?php $i++; ?>
            <?php endforeach; ?> 
        </ul>
    </div>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/lib/ThumbnailGridExpandingPreview/js/grid.js"></script> 
    <script>
                               
        $(document).ready(function(){
			
			  
            if (/MSIE\s([\d.]+)/.test(navigator.userAgent)) {
                //Get the IE version.  This will be 6 for IE6, 7 for IE7, etc...
                version = new Number(RegExp.$1);
                if(version == 8){ 
    				
                    //$(".ch-info").corner("125px");
    					
                    $('.og-grid li a.linkproject').click(function(event){
                       event.preventDefault();
					   window.location = $(this).attr("href");
					   return;
					    
                        $('.og-grid div.heightdefault').css('height', 350);
                        $('.data-content').hide();
    						
                        var heightli = $(this).parent().find('div.data-content').height()+350;
                        $(this).parent().parent().css('height', heightli);
                        $(this).parent().find('div.data-content').slideDown();
						
						$('.og-grid li').removeClass('m_active');
						$(this).parent().addClass('m_active');
						
						$('.og-grid li').each(function(){
								if($(this).hasClass('m_active')){
									return;
								}
								else{
									$(this).find('h3.title').hide();
									$(this).find('.ch-info').hide();
								}
							});
							
                    });
                    $('.og-grid li a.linkproject').hover(
						function(){
							$('.og-grid li').each(function(){
								if($(this).hasClass('m_active')){
									return;
								}
								else{
									$(this).find('h3.title').hide();
									$(this).find('.ch-info').hide();
								}
							});
							
							$(this).find('h3.title').show();
							$(this).find(".ch-info").show();
                    	},
						function(){
							$(this).find('h3.title').hide();
							$(this).find(".ch-info").hide();

							if($(this).parent().hasClass('m_active')){
								$(this).find('h3.title').show();
								$(this).find(".ch-info").show();
							}
							
							
							//var parent = $(this).parent().attr("class");
							
							//.attr("class");
							//alert(parent);
							//alert( $(this).parent().attr("class"));
							}
					);
                    $('.og-closeie8').click(function(){
                        $('.data-content').slideUp();
                        $('.og-grid div.heightdefault').css('height', 350);
                    });
    					
                    $(".og-grid li h3.title").hover(function(){
                        $(this).prev("div.ch-item").find(".ch-info").show();
                        //$(this).prev("div.ch-item").addClass("active");
                        //alert($(this).prev("div.ch-item").attr("class"));
                    },
                    function(){
                        $(this).prev("div.ch-item").removeClass("active");
                    });	
                }else{                        
                    Grid.init();
                }
            }else{
                Grid.init();
            }
            $(".og-grid li h3.title").hover(function(){
                $(this).prev("div.ch-item").addClass("active");
            },
            function(){
                $(this).prev("div.ch-item").removeClass("active");
            });		
                                        		
            $("div.gallery").each(function(){
                var id = $(this).attr("id");
                $(this).find("a").each(function(){
                    $(this).attr("rel",id);
                });
            });	
            $(".gallery a").fancybox({
                openEffect	: 'none',
                closeEffect	: 'none'
            });
                                
        });    	                     	
    </script>
<?php endwhile; // end of the loop.    ?>

<?php get_footer(); ?>
