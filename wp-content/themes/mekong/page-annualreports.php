<?php
/*
  Template Name: Template Annual reports
 */
?>
<?php get_header(); ?>
<?php
if (wpmd_is_phone()) {
    include_once 'mobile/page-annualreports.php';
    get_footer();
    return;
}
?>
<?php while (have_posts()) : the_post(); ?>
                   
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/lib/3DShadingWithBoxShadows/css/component.css" />
<!--
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/lib/3DShadingWithBoxShadows/js/modernizr.custom.js"></script>
-->	

    <div class="page-banner">
        <img src="<?php echo get_field('page_banner'); ?>" />
        <div class="row hide-for-small">
            <div class="large-9 small-20 page-decription">
                <?php echo get_field('introduction_page'); ?>
            </div>
        </div>
    </div>
    <div class="main-content">
        <div class="row">
            <div class="large-1 columns end">&nbsp;</div>
            <div class="large-18 columns end">
                <div class="page-about-team">
                    <div class="large-20 columns end">
                        <div class="title-page-boxmenu">
                            <div class="title-page large-15 left"><?php the_title(); ?></div>
                            <div class="boxmenu large-4 right">
                                <?php the_title(); ?>
                            </div>
                            <div class="icon-boxmenu"></div>
                            <div class="menu-box large-4">
                                <?php
                                $args = array();
                                $args = array(
                                    'theme_location' => 'secondary',
                                    'container' => FALSE,
                                    'menu' => 'Menu about',
                                    'menu_class' => '',
                                    'echo' => true,
                                    'fallback_cb' => '',
                                    'depth' => 4,
                                    //'after' => '<li class="divider"></li>',                        
                                    'menu_id' => '');
                                wp_nav_menu($args);
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="page-about-content">
                        <?php
                        $defaults = array(
                            'post_parent' => $post->ID,
                            'orderby' => 'menu_order',
                            'order' => 'ASC',
                            'post_type' => 'page',
                            'numberposts' => -1,
                            'post_status' => 'publish'
                        );
                        $page_parent = get_children($defaults);
                        ?>
                        
                        <br style="clear:both"/>
                        <div class="note">
							<?php the_content(); ?>
                        </div>
                        <ul class="stage clearfix">
                        
                         <?php foreach ($page_parent as $index => $re): ?>
                               <a href="<?php echo wp_get_attachment_url(get_post_meta($re->ID, 'file_download', true)); ?>">
                                <li class="scene"> 
                                <div class="movie" onclick="return true">
                                	 <div class="poster" style="background:url(http://interactive.com.vn/lab/mekong/wp-content/themes/mekong/images/report_cover.jpg) center center no-repeat; background-size:100%">
                                    <p>
									<?php echo get_post_field( 'cover_text', $re->ID ); ?> 
                                    </p>
                                    </div>
                                    <div class="info"  style="background:url(<?php echo wp_get_attachment_url(get_post_meta($re->ID, 'thumbnail_image', true), 'full'); ?>) center center no-repeat; background-size:100%">
                                    <h2>Download</h2>
                                    </div>
                                </div>
                              	</li>
                                </a>
                            <?php endforeach; ?>
                         </ul>
                          
                          
                       
                    </div>
                </div>
            </div>
        </div>       
    </div>
    
	<br style="clear:both"/>
    
<?php endwhile; // end of the loop.  ?>
<?php get_footer(); ?>

<script>
$("#menu-item-234").addClass("current-menu-item");
</script>