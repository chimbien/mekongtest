<?php
/*
  Template Name: Template Volunteer
 */
?>
<?php get_header(); ?>
<?php
if (wpmd_is_phone()) {
    include_once 'mobile/page-volunteer.php';
    get_footer();
    return;
}
?>
<script>
$("#menu-item-237").addClass("current-menu-item");
</script>
<?php while (have_posts()) : the_post(); ?>
    <div class="banner-donate">
        <img src="<?php echo get_field('page_banner'); ?>" alt="" />
        <div class="row">
            <div class="large-20 columns end">
                <div class="large-5 btn_donate volunteer">
                    <h1><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
    </div>
    <div class="donate-page">    
        <div class="donate-section2">
            <div class="row">
                <div class="large-1 columns end ">&nbsp;</div>
                <div class="large-18 small-20 columns end">
                    <h2>be volunteers</h2>
                    <?php the_content(); ?>
                </div>
                <div class="large-1 columns end ">&nbsp;</div>
            </div>
        </div>
        <div class="link-involved ">
            <div class="row">
                <div class="large-5 columns end">&nbsp;</div>
                <div class="large-12 columns end btn-link-vol">
                    <a href="<?php echo get_page_link(); ?>" class="large-6 small-10 columns end btn_vol1_small">
                        <?php the_title(); ?>                  
                    </a>
                    <span class="large-3 small-10 columns end" style="line-height: 85px;font-size: 31px;color: #cdcdcd;"><?php if (ICL_LANGUAGE_CODE == 'vi'){ echo 'Hoặc';}else{ echo 'OR';} ?></span>
                    <?php
                    $donate = get_page(137);                    
                    ?>
                    <?php if (ICL_LANGUAGE_CODE == 'vi'){
                    $language_text = '?lang=vi';
                }?>
                    <a href="<?php echo get_page_link(137).$language_text; ?>" class="large-6 small-10 columns end btn_vol2_small">
                        <?php echo $donate->post_title;?>                
                    </a>
                </div>
                <div class="large-3 columns end">&nbsp;</div>
            </div>

        </div>

    </div>
<?php endwhile; // end of the loop.  ?>
<?php get_footer(); ?>