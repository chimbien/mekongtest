<?php while (have_posts()) : the_post(); ?>
    <div class="banner-involved">
        <img src="<?php echo get_field('page_banner'); ?>" />        
    </div>
    <div class="involve0 show-for-small">
        <div class="row">
            <div class="small-20 columns">
                <h2><?php the_title(); ?></h2>
            </div>
            <div class="small-10 columns">
                <?php
                $volunteer = get_page(134);
                $donate = get_page(137);
                ?>
                <?php if (ICL_LANGUAGE_CODE == 'vi'){
                    $language_text = '?lang=vi';
                }?>
                <a href="<?php echo get_page_link(134).$language_text; ?>" class="btn_mobile_vol1">
                    <h3><?php echo $volunteer->post_title;?></h3>
                    <p>or work for us</p>
                </a>
            </div>
            <div class="small-10 columns">
                <a href="<?php echo get_page_link(137).$language_text; ?>" class="btn_mobile_vol2">
                    <h3><?php echo $donate->post_title; ?></h3>
                    <p>or become a supporters</p>
                </a>
            </div>
        </div>
    </div>
    <?php
    global $post;
    $args = array(
        'post_type' => 'page',
        'posts_per_page' => -1,
        'post_parent' => $post->ID,
        'order' => 'ASC',
        'orderby' => 'menu_order'
    );       // Declare global $more (before the loop).

    query_posts($args);
    if (have_posts()) : while (have_posts()) : the_post();
            if ((get_post_meta($post->ID, 'position_box', true) == 1) || (get_post_meta($post->ID, 'position_box', true) == 3)) {
                get_template_part('content', 'getinvolced');
            } else {
                //get_template_part('content', 'keystatistics');
            }
        endwhile;
    endif;
    wp_reset_query();
    ?>
    <div class="link-involved hide-for-small">
        <div class="row">
            <div class="large-5 columns end">&nbsp;</div>
            <div class="large-12 columns end btn-link-vol">
                <a href="<?php echo get_page_link(134).$language_text; ?>" class="large-6 small-10 columns end btn_vol1_small">
                    <?php
                    $volunteer = get_page(134);
                    echo $volunteer->post_title;
                    ?>
                </a>
                <span class="large-3 small-10 columns end" style="line-height: 85px;font-size: 31px;color: #cdcdcd;">OR</span>
                <?php
                $donate = get_page(137);
                ?>
                <a href="<?php echo get_page_link(137).$language_text; ?>" class="large-6 small-10 columns end btn_vol2_small">
                    <?php echo $donate->post_title; ?>                   
                </a>
            </div>
            <div class="large-3 columns end">&nbsp;</div>
        </div>
    </div>
<?php endwhile; // end of the loop.  ?>
<?php get_footer(); ?>