<!DOCTYPE html>
<!--[if IE 8]><html class="lt-ie9" xmlns="http://www.w3.org/1999/xhtml"  <?php language_attributes(); ?>><![endif]-->
<!--[if gt IE 8]><!--> <html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>> <!--<![endif]-->

    <head>
        <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
                <title>
                    <?php
                    global $page, $paged;
                    wp_title('|', true, 'right');
// Add the blog name.
                    bloginfo('name');
// Add the blog description for the home/front page.
                    $site_description = get_bloginfo('description', 'display');
                    if ($site_description && ( is_home() || is_front_page() ))
                        echo " | $site_description";
// Add a page number if necessary:
                    if ($paged >= 2 || $page >= 2)
                        echo ' | ' . sprintf(__('Page %s', 'twentyeleven'), max($paged, $page));
                    ?>
                </title>
                <meta charset="utf-8" />
                <meta name="viewport" content="minimum-scale=1,width=device-width" />
                <!--<script src="<?php bloginfo('template_url'); ?>/mobile/lib/jquery-1.8.2.min.js"></script>-->
                <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
                <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/mobile/lib/foundation/css/foundation.css" />
                <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/mobile/lib/foundation/js/vendor/custom.modernizr.js"></script>
                <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/mobile/lib/foundation/js/foundation.min.js"></script> 
                <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/mobile/lib/customSelect/jquery.customSelect.js"></script>
                <?php wp_head(); ?>
                <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/mobile/mobile.css" />
                <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/mobile/mobile_hai.css" />
                <script type="text/javascript">
                    $(document).ready(function(){
                        $('select.styled').customSelect();
                        $("#menu_left").hide();
				
                        $('.boxmenumobile,.icon-boxmenu-mobile').click(function(){                    
                            $('.menu-box-mobile').slideToggle();
                        });

                        $("a.mobile_menu_left").click(function(event){
                            event.preventDefault();
                            var id = $(this).attr("href");
                            $(id).toggle();

                        });
                    });
                </script>
                
                
				<?php if (is_page(141) || is_page_template('page-contact.php')): ?>
                <script type="text/javascript"
                src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAc6wUlFgP0HvHjyCnvVQCwPSmv74jkATs&sensor=true">
                </script>
                <script type="text/javascript">
                function initialize() {
                var myLatlng = new google.maps.LatLng(10.803799,106.640766);  
                var myOptions = {
                    center: myLatlng,
                    zoom: 16,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
                var icon = "http://mekong.org.vn/googlemap_marker.png";
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    icon:icon,
                    title:"Mekong Community Development Center",
                    draggable:true,
                    animation:google.maps.Animation.Bounce
                });
                var contentString = '<h3>Mekong Community Development Center</h3><p style="font-size: 14px;">23 Le Van Huan, Ward 13, Tan binh Dist, Ho Chi Minh City, Vietnam</p>';
                
                var infowindow = new google.maps.InfoWindow({
                    content: contentString
                });
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open(map,marker);
                });
                
                }
                </script>
                <?php endif; ?>
                
                </head>
                <body <?php if (is_page(141) || is_page_template('page-contact.php')): ?>onload="initialize()"<?php endif; ?>>
                    <div class="header-mobile">
                        <div class="row">
                            <div class="small-10 columns">
                                <a href="http://mekong.org.vn" class="logo">
                                    <img src="<?php bloginfo('template_url'); ?>/img/logo_mobile.png" alt="" />
                                </a>
                            </div>
                            <div class="small-6 columns">
                                <!--<a class="switch_language" href="">Tiếng Việt</a>-->
                                <?php do_action('icl_language_selector'); ?>
                            </div>
                            <div class="small-4 right">
                                <a href="#menu_left" class="mobile_menu_left">
                                    <img src="<?php bloginfo('template_url'); ?>/img/icon_menu.png" alt="" />
                                </a>

                            </div>
                        </div>
                    </div>
                    <div class="line"></div>
                    <div id="menu_left">
                        <div class="row">
                            <div class="small-20 columns menu-mobile">
                                <?php
                                $args = array();
                                $args = array(
                                    'theme_location' => 'primary',
                                    'container' => FALSE,
                                    'menu' => 'Main menu',
                                    'menu_class' => 'menu',
                                    'echo' => true,
                                    'fallback_cb' => '',
                                    'depth' => 4,
                                    //'after' => '<li class="divider"></li>',                        
                                    'menu_id' => 'main-nav');
                                wp_nav_menu($args);
                                ?>
                            </div>
                        </div>
                    </div>
                    <br style="clear:both"/>