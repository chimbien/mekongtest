<?php while (have_posts()) : the_post(); ?>
    <div class="main-page-project" style="padding: 0;background: none;">
        <div class="show-for-small">
            <div class="row">
                <div class="small-20 left banner-mobile">
                    <img src="<?php echo get_field('page_banner'); ?>" />
                    <p style="color: #fad13d;text-transform: uppercase;">
                        <?php $intro = trim(str_replace('  ', ' ', strip_tags(get_field('introduction_page'))));?>
                        <?php echo $intro; ?>
                    </p>
                </div>
                <div class="home-moble">
                    <?php
                    $args = array(
                        'post_parent' => $post->ID,
                        'orderby' => 'menu_order',
                        'order' => 'ASC',
                        'post_type' => 'page',
                        'numberposts' => -1,
                        'post_status' => 'publish'
                    );
                    $page_parent = get_children($args);
                    ?>
                    <?php foreach ($page_parent as $index => $re): ?>
                        <div class="content-home-box">
                            <div class="small-6 columns end">
                                <?php echo wp_get_attachment_image(get_post_meta($re->ID, 'thumbnail_image', true), 'full'); ?>
                            </div>
                            <div class="small-14 columns end">
                            <a href="<?php echo get_page_link($re->ID); ?>">
                                <h3><?php echo $re->post_title; ?></h3>
                                <p>
								<?php echo get_post_meta($re->ID, 'description', true); ?>
                            	</p>
                             </a>
                            </div>
                        </div>
                        <hr />
                    <?php endforeach; ?>

                </div>
            </div>
        </div>
    </div>

<?php endwhile; // end of the loop.    ?>
