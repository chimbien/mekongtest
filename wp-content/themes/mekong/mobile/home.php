<?php
$defaults = array(
    'post_parent' => $post->ID,
    'orderby' => 'menu_order',
    'order' => 'ASC',
    'post_type' => 'page',
    'numberposts' => -1,
    'post_status' => 'publish'
);
$page_parent = get_children($defaults);
?>
<?php while (have_posts()) : the_post(); ?>    
    <div>
        <div class="row">

            <div class="small-20 left banner-mobile">
                <img src="<?php echo get_field('page_banner'); ?>" />
                <?php echo get_field('introduction_page'); ?>
            </div>

            <div class="home-moble">
                <?php foreach ($page_parent as $index => $re): ?>
                    <a href="<?php echo get_post_meta($re->ID, 'button_1_url', true); ?>">
                        <div class="content-home-box">
                            <div class="small-6 columns end">
                                <?php $image = wp_get_attachment_image_src(get_post_meta($re->ID, 'thumbnail_mobile', true), 'full'); ?> 
                                <img src="<?php echo $image[0]; ?>" alt="" />
                            </div>
                            <div class="small-14 columns end">
                                <h3><?php echo $re->post_title; ?></h3>
                                <?php $content = apply_filters('the_content', $re->post_content); ?>
                                <?php echo $content; ?>
                            </div>
                        </div>
                    </a>
                    <hr />
                <?php endforeach; ?>
                <?php
                $about = get_page(11);
                ?>
                <div class="link-home">
                    <a href="<?php echo get_page_link(11); ?>"><?php echo $about->post_title; ?> >></a>
                </div>
                <br style="clear:both"/>
                <div class="link-involved">
                    <?php
                    $volunteer = get_page(134);
                    $donate = get_page(137);
                    ?>
                    <center>
                        <a href="<?php echo get_page_link(134); ?>" class="btn_vol1_small">
                            <?php echo $volunteer->post_title; ?>
                        </a>
                        &nbsp;OR&nbsp;
                        <a href="<?php echo get_page_link(137); ?>" class="btn_vol2_small">
                            <?php echo $donate->post_title; ?>                  
                        </a>
                    </center>
                </div>

            </div>
        </div>
    </div>
<?php endwhile; ?>