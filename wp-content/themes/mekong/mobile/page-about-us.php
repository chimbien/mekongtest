<?php while (have_posts()) : the_post(); ?>
    <div class="box-headine">
        <h2><?php the_title(); ?></h2>
    </div>
    <div class="page-banner">
        <img src="<?php echo get_field('page_banner'); ?>" />
    </div>
    <div class="page-decription">
        <?php echo get_field('introduction_page'); ?>
    </div>

    </div>
    <div class="main-content">
        <?php the_content(); ?>
    </div>
    
<?php endwhile; // end of the loop.  ?>
<?php include 'about-sub-menu.php'; ?>