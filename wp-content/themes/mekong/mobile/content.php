<div class="box-new">
    <div class="large-20 small-20 columns end">
        <h3>
            <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
        </h3>     
    </div>

    <?php if (has_post_thumbnail() && !post_password_required()) : ?>
        <a href="<?php the_permalink(); ?>" rel="bookmark">
            <?php the_post_thumbnail('full'); ?>
        </a>
    <?php endif; ?>
    <?php the_content('Read more'); ?>
    <p class="date-published"><?php echo get_the_time('M d, Y', $post->ID); ?></p>
    
</div>
<hr />
