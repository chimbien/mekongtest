<?php while (have_posts()) : the_post(); ?>
    <div class="page-banner">
        <div id="map_canvas" style="width:100%; height:250px"></div>
    	</div>
    </div>
    <div class="main-page-contact">
        <div class="box-address">
            <h3><?php echo get_post_meta($post->ID, 'company_name', true); ?></h3>
            <p><?php echo get_post_meta($post->ID, 'address', true); ?></p>
            <div class="left">
                <img src="<?php bloginfo('template_url'); ?>/mobile/images/icon_phone.png" alt="" /> <p><?php echo get_post_meta($post->ID, 'phone', true); ?></p>
                <img src="<?php bloginfo('template_url'); ?>/mobile/images/icon_fax.png" alt="" />  <p><?php echo get_post_meta($post->ID, 'fax', true); ?></p>
            </div>
            <div class="right">
                <img src="<?php bloginfo('template_url'); ?>/mobile/images/icon_email.png" alt="" />  <p><?php echo get_post_meta($post->ID, 'email', true); ?></p>
                <img src="<?php bloginfo('template_url'); ?>/mobile/images/icon_website.png" alt="" />  <p><?php echo get_post_meta($post->ID, 'website', true); ?></p>
            </div>
            <br style="clear:both"/>
        </div>
        <div class="face-book" style="text-align: center;">
            <!--<img src="<?php bloginfo('template_url'); ?>/mobile/images/face_book.jpg" />-->
<?php echo get_field('facebook_like'); ?> 
        </div>
        <div class="social" style="display:none;">
            <a class="facebook" href="<?php echo get_post_meta($post->ID, 'link_facebook', true); ?>"><img src="<?php bloginfo('template_url'); ?>/mobile/images/contact-icon-facebook.png" /></a>
            <a class="google" href="<?php echo get_post_meta($post->ID, 'link_google', true); ?>"><img src="<?php bloginfo('template_url'); ?>/mobile/images/contact-icon-google.png" /></a>
            <a class="twiter" href="<?php echo get_post_meta($post->ID, 'link_twitter', true); ?>"><img src="<?php bloginfo('template_url'); ?>/mobile/images/contact-icon-twitter.png" /></a>
        </div>
        <br style="clear:both"/>
        <?php the_content(); ?>
    </div>
<?php endwhile; ?>