<?php //while (have_posts()) : the_post();     ?>
<div class="box-headine">
    <h2><?php the_title(); ?></h2>
</div>
<script>
    $("#menu-item-236").addClass("current-menu-item");
</script>
<div class="row">
    <div class="small-20 columns news-list">
        <div class="tab">
            <?php
            $categories = get_the_category();
            $currentCat = $categories[0]->cat_ID;
            //echo "cat is ".$currentCat;
            ?>
            <?php if (ICL_LANGUAGE_CODE == 'vi'): ?>
                <a href="<?php echo get_page_link(32) . '?lang=vi'; ?>"><li class="new-all large-5 small-7 columns end active">Tất cả </li></a>
                <a href="<?php echo get_category_link(3); ?>"><li class="new-only large-5 small-7 columns end  <?php
            if ($currentCat == 16): echo "active";
            endif;
                ?>">Tin tức</li></a>
                <a href="<?php echo get_category_link(4); ?>"><li class="new-photo large-5 small-6 columns end  <?php
                                                              if ($currentCat == 17): echo "active";
                                                              endif;
                ?>">Hình ảnh & Video</li></a>
                <?php else: ?>

                <a href="<?php echo get_page_link(32); ?>"><li class="new-all large-5 small-7 columns end active">All</li></a>
                <a href="<?php echo get_category_link(3); ?>"><li class="new-only large-5 small-6 columns end <?php
                if ($currentCat == 3): echo "active";
                endif;
                    ?>">News</li></a>
                <a href="<?php echo get_category_link(4); ?>"><li class="new-photo large-5 small-6 columns end <?php
                                                              if ($currentCat == 4): echo "active";
                                                              endif;
                    ?>">Photos & Videos</li></a>
                <?php endif; ?>
        </div>
    </div>
    <div class="small-20 columns"></div>
</div>


<div class="main-page-new">
    <?php
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args2 = array(
        'cat' => '3,4',
        'posts_per_page' => 5,
        'post_status' => 'publish',
        'paged' => $paged
    );
    global $more;    // Declare global $more (before the loop).
    global $post;
    $more = 0;
    /* $result = get_posts($args);
      foreach ($result as $r):
      $format = get_post_format($r->ID);
      if($format == 'gallery'){
      echo '<h3>'.$r->post_title.' Gallery </h3>';
      }elseif($format == 'video') {
      echo '<h3>'.$r->post_title.' Video </h3>';
      }else{
      echo '<h3>'.$r->post_title.' Bai Viet </h3>';
      }

      /*if ($taxonomy[0]->slug == 'slideshow'):
      $images = wp_get_attachment_image_src($r->ID, 'full');
      if ($images):
      ?>
      <img src="<?php echo $images[0]; ?>" alt="Slideshow"/>
      <?php
      endif;
      endif;
      endforeach; */
    query_posts($args2);
    if (have_posts()) : while (have_posts()) : the_post();
            ?>
            <div class="box-new">
                <div class="large-20 small-20 columns end">
                    <h3>
                        <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
                    </h3>     
                </div>

                <?php if (has_post_thumbnail() && !post_password_required()) : ?>
                    <a href="<?php the_permalink(); ?>" rel="bookmark">
                        <?php the_post_thumbnail('full'); ?>
                    </a>
                <?php endif; ?>
                <?php the_content('Read more'); ?>
                <p class="date-published"><?php echo get_the_time('M d, Y', $post->ID); ?></p>

            </div>
            <hr />
        <?php
        endwhile;
    endif;
    wp_pagenavi();
    wp_reset_query();
    ?>

</div>
<?php //endwhile; // end of the loop.   ?>
