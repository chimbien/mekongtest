<?php while (have_posts()) : the_post(); ?>
    <div class="box-headine">
        <h2>News & Events</h2>
    </div>
    <div class="detail-page-new">
        <div class="box-new">
            <h3><a href="#"><?php the_title(); ?></a></h3>
            <p class="date-published"><a href="#"><?php echo get_the_time('M d, Y', $post->ID); ?></a></p>
            <br style="clear:both"/>
            <?php the_content(); ?>
        </div>
        <hr/>

        <ul class="other-new">
            <li class="title">Other news</li>
            <?php
            global $post;
            $categories = get_the_category($post->ID);
            //print_r($categories[0]->term_id);
            ?>
            <?php
            $args = array(
                'cat' => $categories[0]->term_id,
                'posts_per_page' => 7,
                'post_type' => 'post',
                'post_status' => 'publish'
            );

            query_posts($args);
            ?>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <li>
                        <span><?php echo get_the_time('d/m/Y', $post->ID); ?> : </span><br />
                        <a href="<?php echo the_permalink() ?>"><?php the_title(); ?></a>
                    </li>
                    <?php
                endwhile;
            endif;
            wp_reset_query();
            ?>
        </ul>     


    </div>
  
<?php endwhile; // end of the loop.  ?>