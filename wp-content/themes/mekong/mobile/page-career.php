<?php while (have_posts()) : the_post(); ?>
    <div class="box-headine">
        <h2><?php the_title(); ?></h2>
    </div>
    <div class="page-banner">
        <img src="<?php echo get_field('page_banner'); ?>" />
    </div>
    <div class="main-content">
        <?php $content = apply_filters("the_content", $post->post_content); ?>
        <?php echo $content; ?>
    </div>
<?php endwhile; // end of the loop.  ?>
<?php include 'about-sub-menu.php'; ?>