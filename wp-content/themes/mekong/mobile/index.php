<div>
    <div class="row">
        <div class="small-20 left banner-mobile">
            <img src="<?php bloginfo('template_url'); ?>/mobile/images/mobile_home.jpg" alt="" />
            <p>Transforming Lives and Communities</p>
        </div>

        <div class="home-moble">
            <a href="#">
                <div class="content-home-box">
                    <div class="small-6 columns end">
                        <img src="<?php bloginfo('template_url'); ?>/mobile/img/thumb_home1.png" alt="" />
                    </div>
                    <div class="small-14 columns end">

                        <h3>Microfinance</h3>
                        <p>Lorem Ipsum. Proin gravida nibh vel velit auctor acfgjyre</p>

                    </div>
                </div>
            </a>
            <hr />
            <div class="content-home-box">
                <div class="small-6 columns end">
                    <img src="<?php bloginfo('template_url'); ?>/mobile/img/thumb_home2.png" alt="" />
                </div>
                <div class="small-14 columns end">
                    <h3>Mobile School</h3>
                    <p>Lorem Ipsum. Proin gravida nibh vel velit auctor acfgjyre aliqAenean</p>
                </div>
            </div>
            <hr />
            <div class="content-home-box">
                <div class="small-6 columns end">
                    <img src="<?php bloginfo('template_url'); ?>/mobile/img/thumb_home3.png" alt="" />
                </div>
                <div class="small-14 columns end">
                    <h3>Scholarship</h3>
                    <p>Lorem Ipsum. Proin gravida nibh vel velit auctor acfgjyre aliqAenean</p>
                </div>
            </div>
            <hr />
            <div class="content-home-box">
                <div class="small-6 columns end">
                    <img src="<?php bloginfo('template_url'); ?>/mobile/img/thumb_home4.png" alt="" />
                </div>
                <div class="small-14 columns end">
                    <h3>School building</h3>
                    <p>Lorem Ipsum. Proin gravida nibh vel velit auctor acfgjyre aliqAenean</p>
                </div>
            </div>
            <hr />
            <div class="content-home-box">
                <div class="small-6 columns end">
                    <img src="<?php bloginfo('template_url'); ?>/mobile/img/thumb_home5.png" alt="" />
                </div>
                <div class="small-14 columns end">
                    <h3>Financial Education</h3>
                    <p>Lorem Ipsum. Proin gravida nibh vel velit auctor acfgjyre aliqAenean</p>
                </div>
            </div>
            <hr />
            <div class="content-home-box">
                <div class="small-6 columns end">
                    <img src="<?php bloginfo('template_url'); ?>/mobile/img/thumb_home6.png" alt="" />
                </div>
                <div class="small-14 columns end">
                    <h3>Mobile Denal Clinics</h3>
                    <p>Lorem Ipsum. Proin gravida nibh vel velit auctor acfgjyre aliqAenean</p>
                </div>
            </div>
            <hr />
            <div class="content-home-box">
                <div class="small-6 columns end">
                    <img src="<?php bloginfo('template_url'); ?>/mobile/img/thumb_home7.png" alt="" />
                </div>
                <div class="small-14 columns end">
                    <h3>Mobile Vocational Centers</h3>
                    <p>Lorem Ipsum. Proin gravida nibh vel velit auctor acfgjyre aliqAenean</p>
                </div>
            </div>
            <hr />
            <div class="link-home">
                <a href="#">About Us >></a>
            </div>
            <br style="clear:both"/>
            <div class="link-involved">
                <center>
                    <a href="volunteer.php" class="btn_vol1_small">
                        Volunteer               
                    </a>
                    &nbsp;OR&nbsp;
                    <a href="donate.php" class="btn_vol2_small">
                        Donate                
                    </a>
                </center>
            </div>

        </div>
    </div>
</div>