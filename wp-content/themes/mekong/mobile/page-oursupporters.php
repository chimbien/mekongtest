<?php while (have_posts()) : the_post(); ?>
    <div class="box-headine">
        <h2><?php the_title(); ?></h2>
    </div>
    <div class="page-banner">
        <img src="<?php echo get_field('page_banner'); ?>" />
    </div>
    <div class="page-decription">
        <?php echo get_field('introduction_page'); ?>
    </div>
    <div class="main-content">
        <?php the_content(); ?>
        <ul class="small-block-grid-3 doitac">
            <?php
            $defaults = array(
                'post_parent' => $post->ID,
                'orderby' => 'menu_order',
                'order' => 'ASC',
                'post_type' => 'page',
                'numberposts' => -1,
                'post_status' => 'publish'
            );
            $page_parent = get_children($defaults);
            ?>
            <?php foreach ($page_parent as $index => $re): ?>
                <li> <a href="<?php echo get_post_meta($re->ID, 'link', true); ?>"><?php echo wp_get_attachment_image(get_post_meta($re->ID, 'logo', true), 'full'); ?></a> </li>
            <?php endforeach; ?>
        </ul>
    </div>    
<?php endwhile; // end of the loop.  ?>
<?php include 'about-sub-menu.php'; ?>