<?php while (have_posts()) : the_post(); ?>
    <div class="banner-donate">
        <img src="<?php echo get_field('page_banner'); ?>" alt="" />
        <div class="title_bg"></div>
        <h2><?php the_title(); ?></h2>
    </div>
    <div class="volunteer-page">
        <h2>BE VOLUNTEERS</h2>
        <?php the_content(); ?>
    </div>
    <div class="link-involved" style="background-color:white;">
        <center>
            <?php
            $donate = get_page(137);            
            ?>
            <?php if (ICL_LANGUAGE_CODE == 'vi'){
                    $language_text = '?lang=vi';
                }?>
            <a href="<?php echo get_page_link(); ?>" class="btn_vol1_small">
                <?php the_title(); ?>               
            </a>
            &nbsp;OR&nbsp;
            <a href="<?php echo get_page_link(137).$language_text; ?>" class="btn_vol2_small">
                <?php echo $donate->post_title;?>                
            </a>
        </center>
    </div>
<?php
endwhile; // end of the loop.  ?>