<?php while (have_posts()) : the_post(); ?>
    <div class="box-headine">
        <h2><?php the_title(); ?></h2>
    </div>
    <div class="page-banner">
        <img src="<?php echo get_field('page_banner'); ?>" />
    </div>
    <div class="keystats">
        <center>
            <div class="facts-figures-headline">
                <div class="bg-left"></div><div class="bg-center"><span>FACTS & FIGURES</span></div><div class="bg-right"></div>
            </div>
        </center>


        <div class="gender">
            <div class="row">
                <div class="image-gender">
                    <ul class="small-block-grid-4">
                        <li> <img src="<?php bloginfo('template_url'); ?>/mobile/images/women.png" alt="" /></li>
                        <li> <img src="<?php bloginfo('template_url'); ?>/mobile/images/women.png" alt="" /></li>
                        <li> <img src="<?php bloginfo('template_url'); ?>/mobile/images/men.png" alt="" /></li>
                        <li> <img src="<?php bloginfo('template_url'); ?>/mobile/images/men.png" alt="" /></li>
                    </ul>
                    <div class="info_left"> 
                        <h2><?php echo get_post_meta($post->ID, 'gender_percent', true); ?>%</h2>
                    </div>
                    <div class="info_right">
                        <p><?php echo get_post_meta($post->ID, 'gender_percent_text', true); ?></p>
                    </div> 
                </div>
            </div>
        </div> 




        <div class="school">
            <div class="row">
                <div class="info-left">                            
                    <h2>
                        <?php
                        if (get_post_meta($post->ID, 'mobile_school', true) < 10) {
                            echo '0' . get_post_meta($post->ID, 'mobile_school', true);
                        } else {
                            echo get_post_meta($post->ID, 'mobile_school', true);
                        }
                        ?>
                    </h2>
                    <p><?php echo get_post_meta($post->ID, 'mobile_school_text', true); ?></p>
                    <h2 style="font-size: 40px;margin-bottom: 6px;text-align: center;margin-top: -10px;">
                        <?php
                        if (get_post_meta($post->ID, 'provinces', true) < 10) {
                            echo '0' . get_post_meta($post->ID, 'provinces', true);
                        } else {
                            echo get_post_meta($post->ID, 'provinces', true);
                        }
                        ?>
                    </h2>
                    <p><strong style="font-size: 10px;text-transform: uppercase;"><?php echo get_post_meta($post->ID, 'provinces_text', true); ?></strong></p>
                </div>
                <div class="info-right">
                    <ul class="small-block-grid-3">
                        <?php for ($i = 0; $i < get_post_meta($post->ID, 'mobile_school', true); $i++): ?>
                            <li><img src="<?php bloginfo('template_url'); ?>/mobile/images/icon_school.png" alt="" /></li>
                        <?php endfor; ?>
                    </ul>

                </div>
            </div>
        </div>


        <div class="scholarship"> 
            <img src="<?php bloginfo('template_url'); ?>/mobile/images/bieudo.png" alt="" />
            <p><?php echo get_post_meta($post->ID, 'bike_text', true); ?></p>
        </div>

        <div class="number-student">
            <div class="background">
                <h2 class="number-student-mobile-h2"><?php echo get_post_meta($post->ID, 'students_joined', true); ?></h2>
                <p><span style="color:#3dd0ba">students</span> joined<br />our training program<br />every year</p>
            </div>
        </div>


        <div class="student">   
            <div class="background">                     
                <div class="number-pass large-7 columns end">                            
                    <h2><?php echo get_post_meta($post->ID, 'students_passed', true); ?>%</h2>
                    <p>Students <span style="font-size: 20px;">passed</span><br /> &nbsp; &nbsp; the final exam</p>
                </div>
            </div>
        </div>

        <div class="toilet">
            <div class="left-info">                            
                <img src="<?php bloginfo('template_url'); ?>/mobile/images/icon_toilet.png" alt="" />
            </div>
            <div class="right-info">
                <h2><?php echo get_post_meta($post->ID, 'toilet_access', true); ?>%</h2>
                <p><?php echo get_post_meta($post->ID, 'toilet_access_text1', true); ?></p>
                <br style="clear:both"/>
                <?php $notolet = 100 - get_post_meta($post->ID, 'toilet_access', true); ?>
                <h2><?php echo $notolet ?>%</h2>
                <p><?php echo get_post_meta($post->ID, 'toilet_access_text2', true); ?></p>
            </div>
            <br style="clear:both"/>

        </div>

        <div class="toilet_map">
            <?php echo get_field('content_map_viet_nam'); ?>
        </div>   
    </div>
<?php endwhile; ?>
<?php include 'about-sub-menu.php'; ?>