<?php while (have_posts()) : the_post(); ?>
    <div class="box-headine">
        <h2><?php the_title(); ?></h2>
    </div>
    <div class="page-banner">
        <img src="<?php echo get_field('page_banner'); ?>" />
    </div>

    <div class="main-content">
        <div class="row">
            <?php
            $defaults = array(
                'post_parent' => $post->ID,
                'orderby' => 'menu_order',
                'order' => 'ASC',
                'post_type' => 'page',
                'numberposts' => -1,
                'post_status' => 'publish'
            );
            $page_parent = get_children($defaults);
            ?>
            <ul class="small-block-grid-2 reports">
                <?php foreach ($page_parent as $index => $re): ?>
                    <li><a href="<?php echo get_post_meta($re->ID, 'file_download', true); ?>">
                        <?php echo wp_get_attachment_image(get_post_meta($re->ID, 'thumbnail_image', true), 'full'); ?></a>
                    </li>
                <?php endforeach; ?>
                <!--<li>
                    <a href="#">
                        <img src="images/about_report_cover.jpg" alt="" />
                        <center><p>ANNUAL REPORT <br/><strong>2010</strong></p></center>
                    </a>
                </li>-->                
            </ul>
        </div>
    </div>
<?php endwhile; // end of the loop.  ?>
<?php include 'about-sub-menu.php'; ?>
