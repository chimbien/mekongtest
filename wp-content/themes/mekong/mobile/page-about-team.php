<?php while (have_posts()) : the_post(); ?>
    <div class="box-headine">
        <h2><?php the_title(); ?></h2>
    </div>
    <div class="page-banner">
        <img src="<?php echo get_field('page_banner'); ?>" />
    </div>
    <div class="main-content">
        <?php
        $defaults = array(
            'post_parent' => $post->ID,
            'orderby' => 'menu_order',
            'order' => 'ASC',
            'post_type' => 'page',
            'numberposts' => -1,
            'post_status' => 'publish'
        );
        $page_parent = get_children($defaults);
        ?>
        <?php foreach ($page_parent as $index => $re): ?>                          
            <div class="member">
                <div class="thumbnail">
                    <?php $images = wp_get_attachment_image_src(get_post_thumbnail_id($re->ID), 'full'); ?>
                    <img src="<?php echo $images[0]; ?>"  alt="" />
                </div>
                <div class="name"><?php echo $re->post_title; ?></div>
                <div class="position"><?php echo get_post_meta($re->ID, 'position', true); ?></div>
                <div class="detail">
                    <?php $content = apply_filters('the_content', $re->post_content); ?>
                    <?php echo $content; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

<?php endwhile; ?>
<?php include 'about-sub-menu.php'; ?>