<?php while (have_posts()) : the_post(); ?>
    <div class="project-headline">
        <h3><?php the_title(); ?></h3>
    </div>
    <div class="main-page-project">

        <div class="box-project-detail">
            <?php the_content(); ?>
            <?php if (get_post_meta($post->ID, 'link', true)): ?>
                <p><a href="<?php echo get_post_meta($post->ID, 'link', true); ?>" target="_blank">Read more</a></p>
            <?php endif; ?>
            <a href="#"class="readmore hide-for-small">Read more</a>
            <!--
            <hr/>
            <h5>IMAGES GALLERY</h5>
            <a href="#"><img src="images/g1.jpg" alt="" /></a>-->

        </div>
    </div>   
<?php
endwhile; // end of the loop.     ?>