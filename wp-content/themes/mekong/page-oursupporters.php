<?php
/*
  Template Name: Template Our Supporters
 */
?>
<?php get_header(); ?>
<?php
if (wpmd_is_phone()) {
  include_once 'mobile/page-oursupporters.php';
  get_footer();
  return;
}
?>
<?php while (have_posts()) : the_post(); ?>
    <div class="page-banner">
        <img src="<?php echo get_field('page_banner'); ?>" />
        <div class="row">
            <div class="large-9 small-20 page-decription">
                <?php echo get_field('introduction_page'); ?>
            </div>
        </div>

    </div>
    <div class="main-content">
        <div class="row">
            <div class="large-1 columns end">&nbsp;</div>
            <div class="show-for-small small-20 columns end">
                <?php echo get_field('introduction_page'); ?>
            </div>
            <div class="large-18 columns end">
                <div class="page-about-team">
                    <div class="large-20 columns end">
                        <div class="title-page-boxmenu">
                            <div class="title-page large-15 left"><?php the_title(); ?></div>
                            <div class="boxmenu large-4 right">
                                <?php the_title(); ?>
                            </div>
                            <div class="icon-boxmenu"></div>
                            <div class="menu-box large-4">
                                <?php
                                $args = array();
                                $args = array(
                                    'theme_location' => 'secondary',
                                    'container' => FALSE,
                                    'menu' => 'Menu about',
                                    'menu_class' => '',
                                    'echo' => true,
                                    'fallback_cb' => '',
                                    'depth' => 4,
                                    //'after' => '<li class="divider"></li>',                        
                                    'menu_id' => '');
                                wp_nav_menu($args);
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="page-our-supporters">
                        <div class="large-15 small-20 intro">
                            <?php the_content(); ?>
                        </div>
                        <?php
                        $defaults = array(
                            'post_parent' => $post->ID,
                            'orderby' => 'menu_order',
                            'order' => 'ASC',
                            'post_type' => 'page',
                            'numberposts' => -1,
                            'post_status' => 'publish'
                        );
                        $page_parent = get_children($defaults);
                        ?>
                        <ul class="large-block-grid-3 doitac">
                            <?php foreach ($page_parent as $index => $re): ?>
                            <li> <a href="<?php echo get_post_meta($re->ID, 'link', true); ?>"><?php echo wp_get_attachment_image(get_post_meta($re->ID, 'logo', true), 'full'); ?></a> </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="large-1 columns end">&nbsp;</div>
        </div>
    </div>
<?php endwhile; // end of the loop.  ?>
<?php get_footer(); ?>

<script>
$("#menu-item-234").addClass("current-menu-item");
</script>