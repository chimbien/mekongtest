<?php
if (wpmd_is_phone()) {
    include_once 'mobile/header.php';
    return;
}
?>
<!DOCTYPE html>
<!--[if gt IE 8]><html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 8]><html class="lt-ie9" xmlns="http://www.w3.org/1999/xhtml"  <?php language_attributes(); ?>><![endif]-->
<head profile="http://gmpg.org/xfn/11">
    <!--[if IE 8]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <![endif]-->
    <meta content="width=device-width" name="viewport" />
    <base href="<?php bloginfo('url'); ?>" />
    <title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
    <link href="<?php bloginfo('template_url'); ?>/images/favicon.png" type="image/x-icon" rel="shortcut icon">
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<meta charset="utf-8">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>-->
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>


    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/lib/3dbox/css/normalize.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/lib/foundation/css/foundation.css">
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/lib/foundation/js/vendor/custom.modernizr.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/lib/foundation/js/foundation.min.js"></script> 
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/lib/fancybox2/jquery.fancybox.pack.js?v=2.1.5"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/lib/ThumbnailGridExpandingPreview/js/modernizr.custom.js"></script>    
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/lib/customSelect/jquery.customSelect.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/lib/jquery.touchSwipe.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/lib/cufont/cufon-yui.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/lib/cufont/giorgiosans-cufont_700.font.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/lib/cufont/AvenirNext_500.font.js"></script>
    <!--[if IE 8]>
            <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/ie8-grid-foundation-4.css" />
                       <link rel="stylesheet" media="screen and (max-width: 1366px)" href="<?php bloginfo('template_url'); ?>/css/IE8.css" />
            <![endif]-->

    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/lib/calendar/css/ui-lightness/jquery-ui-1.10.3.custom.css" />
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/lib/calendar/jquery-ui-1.10.3.custom.min.js"></script>
    <?php wp_head(); ?>
    <style type="text/css" media="screen">
        @import url("<?php bloginfo('template_url'); ?>/style.css");
        @import url("<?php bloginfo('template_url'); ?>/screens.css");
    </style>


    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/m2.css" />
     <!--[if IE 9]>
            <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/IE9.css" />
     <![endif]-->
    <!--[if IE 8]>
            <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/ie8-grid-foundation-4.css" />
                        <link rel="stylesheet" media="screen and (max-width: 1366px)" href="<?php bloginfo('template_url'); ?>/css/IE8.css" />
        <link rel="stylesheet" media="screen and (max-width: 1366px)" href="<?php bloginfo('template_url'); ?>/css/IE8-m.css" />
        <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/lib/respond.src.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('.ch-item').hover(function(){                    
                    $(this).find('.ch-info').show();
                    $(this).find('h3.title').show;
                },function(){
                    $(this).find('.ch-info').hide();
                    $(this).find('h3.title').hide();
                });
            });
        </script>
    <![endif]-->
	
    <!--[if !IE]><!-->
	<script type="text/javascript">
		﻿if (/*@cc_on!@*/false) {
			document.documentElement.className+=' ie10';
			
		}
	</script>
    <!--<!endif]-->
			
    <script type="text/javascript">
		

		
		
        $(document).ready(function(){
           
            $('.box-project').hover(function(){                    
                $(this).find('a.showproject').show();
            },function(){
                $(this).find('a.showproject').hide();
            });
            $('.boxmenu,.icon-boxmenu').click(function(){                    
                $('.menu-box').slideToggle();
            });
            $('.boxmenumobile,.icon-boxmenu-mobile').click(function(){                    
                $('.menu-box-mobile').slideToggle();
            });
            $('select.styled').customSelect();
            $("#menu_left").hide();
                        
            //$('.gallery-item a').fancybox();
                        
            $(".birthday input").datepicker({				
                clickInput:true,				
                dateFormat: "dd/mm/yy",
                changeMonth: true,
                changeYear: true,
                yearRange: "1950:2020"
            }); 
            $("a.mobile_menu_left").click(function(event){
                event.preventDefault();
                var id = $(this).attr("href");
                $(id).toggle();

            });       
        });
    </script>
    <?php if (is_page(32) || is_category(3) || is_category(4)): ?>
        <?php
        $categories = get_the_category();
        $currentCat = $categories[0]->cat_ID;
        //echo "cat is ".$currentCat;
        if ($currentCat) {
            $page_cur = $currentCat;
        } else {
            $page_cur = "3,4";
        }
        ?>
        <script type="text/javascript">
            $(document).ready(function(){
                var hii = 0;
                $(window).scroll(function() {
                                                
                    if($(window).scrollTop() + $(window).height() == $(document).height()) {
                        hii= hii+1;
                        $('.page-new').append('<div class="large-20 left list-new-load'+hii+'">');
                        $.post('http://<?php echo $_SERVER[HTTP_HOST]; ?>/ajax-page/',{
                            "offset": hii,
                            "post_id": "<?php echo $page_cur; ?>" 
                        }, function(data) {
                            $('.list-new-load'+hii).html(data);
                                                        
                            ;                                    });                                    
                        $('.page-new').append("</div>");
                    }
                                                
                }); 
            });
        </script>
    <?php endif; ?>
    <?php if (is_page(141) || is_page_template('page-contact.php')): ?>
        <script type="text/javascript"
                src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAc6wUlFgP0HvHjyCnvVQCwPSmv74jkATs&sensor=true">
        </script>
        <script type="text/javascript">
            function initialize() {
                var myLatlng = new google.maps.LatLng(10.803799,106.640766);  
                var myOptions = {
                    center: myLatlng,
                    zoom: 16,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
                var icon = "http://mekong.org.vn/googlemap_marker.png";
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    icon:icon,
                    title:"Mekong Community Development Center",
                    draggable:true,
                    animation:google.maps.Animation.Bounce
                });
                var contentString = '<h3>Mekong Community Development Center</h3><p style="font-size: 14px;">23 Le Van Huan, Ward 13, Tan binh Dist, Ho Chi Minh City, Vietnam</p>';

                var infowindow = new google.maps.InfoWindow({
                    content: contentString
                });
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open(map,marker);
                });

            }
        </script>
    <?php endif; ?>
</head>
<body <?php if (is_page(141) || is_page_template('page-contact.php')): ?>onload="initialize()"<?php endif; ?>>
   
    <div style="width: 100%; overflow: hidden;">
        <div class="header">
            <div class="row">
                <div class="large-1 columns end">&nbsp;</div>
                <div class="large-3 columns end">
                    <a class="" href="<?php echo home_url(); ?>">
                        <h1 class="logo"></h1>
                        <!--<img src="<?php bloginfo('template_url'); ?>/images/mekong_logo.png" alt="" />-->
                    </a>
                </div>
                <div class="large-11 columns end">
                    <div class="main-menu">                                        
                        <?php
                        $args = array();
                        $args = array(
                            'theme_location' => 'primary',
                            'container' => FALSE,
                            'menu' => 'Main menu',
                            'menu_class' => 'menu',
                            'echo' => true,
                            'fallback_cb' => '',
                            'depth' => 4,
                            //'after' => '<li class="divider"></li>',                        
                            'menu_id' => 'main-nav');
                        wp_nav_menu($args);
                        ?>
                    </div>
                </div>
                <div class="large-4 columns end">
                    <div class="search-box" style="float:right !important;">
                        <div class="language" style="visibility:visible;">
                            <!--<a href="#">Tiếng Việt</a> / <a href="#" class="active">English</a>-->
                            <?php do_action('icl_language_selector'); ?>
                        </div>
                        <div class="form-search">
                            <!-- <form action="" method="post">
                                 <input type="text" value="" name="keyword" placeholder="Search..." class="input_search" />
                                 <input type="submit" class="btn_search" value="" >
                             </form>-->
                            <?php get_search_form(); ?>
                        </div>
                    </div>
                    <div class="social" style="display:none;">
                        <?php $contact = get_page(141); ?>
                        <a class="facebook-ico" href="<?php echo get_post_meta($contact->ID, 'link_facebook', true); ?>">
                        </a>
                        <a class="google-ico" href="<?php echo get_post_meta($contact->ID, 'link_twitter', true); ?>">
                        </a>
                        <a class="twitter-ico" href="<?php echo get_post_meta($contact->ID, 'link_google', true); ?>">
                        </a>
                    </div>
                </div>
                <div class="large-1 columns end hide-for-small hide-for-medium">&nbsp;</div>
            </div>
        </div>
        <div class="line"></div>