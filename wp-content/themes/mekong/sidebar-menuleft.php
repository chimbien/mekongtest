<ul class="side-nav">
    <?php
    $args = array(
        'order' => 'ASC',
        'orderby' => 'menu_order',
        'post_type' => 'nav_menu_item',
        'post_status' => 'publish',
        'output' => ARRAY_A,
        'output_key' => 'menu_order',
        'nopaging' => true,
        'update_post_term_cache' => false);
    ?>
    <?php
    $items = wp_get_nav_menu_object('left-menu');
    $menuitems = wp_get_nav_menu_items($items->term_id, array('order' => 'DESC'));
    ?> 
    <?php
    $count = 0;
    $submenu = false;

    foreach ($menuitems as $item):
        // get page id from using menu item object id
        $id = get_post_meta($item->ID, '_menu_item_object_id', true);
        // get icon for menu       
        $icon_id = get_post_meta($id, 'icon', true);
        $image_attributes = wp_get_attachment_image_src($icon_id);

        // set up a page object to retrieve page data
        $page = get_page($id);
        $link = get_page_link($id);

        // item does not have a parent so menu_item_parent equals 0 (false)
        if (!$item->menu_item_parent):
            // save this id for later comparison with sub-menu items
            $parent_id = $item->ID;
            ?>
            <li class="item">
                <img src="<?php echo $image_attributes[0]; ?>" alt="" />
                <a href="<?php echo $link; ?>" class="title">
                    <?php echo $page->post_title; ?>
                </a>

            <?php endif; ?>

            <?php if ($parent_id == $item->menu_item_parent): ?>

                <?php if (!$submenu): $submenu = true; ?>
                    <ul class="sub-menu">
                    <?php endif; ?>

                    <li class="item">
                        <a href="<?php echo $link; ?>" class="title"><?php echo $page->post_title; ?></a>                        
                    </li>
                    <?php if ($menuitems[$count + 1]->menu_item_parent != $parent_id && $submenu): ?>
                    </ul>
                    <?php
                    $submenu = false;
                endif;
                ?>

            <?php endif; ?>

            <?php if ($menuitems[$count + 1]->menu_item_parent != $parent_id): ?>
            </li>
            <?php
            $submenu = false;
        endif;
        ?>

        <?php $count++; ?>
        <li class="divider">
        <?php endforeach;
        ?>

    <li class="subscribe_box"><img src="<?php bloginfo('template_url'); ?>/images/subscribe.png"/><a href="#">SUBSCRIBE</a>

        <br style="clear:both"/>
        <form action='http://icenewsletter.activehosted.com/proc.php' method='post' id='_form_53' accept-charset='utf-8' enctype='multipart/form-data'>
            <input type='hidden' name='f' value='53'>
            <input type='hidden' name='s' value=''>
            <input type='hidden' name='c' value='0'>
            <input type='hidden' name='m' value='0'>
            <input type='hidden' name='act' value='sub'>
            <input type='hidden' name='nlbox[]' value='4'>
            <table>
                <tr>
                    <td class="first"><label>Name</label></td>
                    <td><input type="text" name='fullname'/></td>
                </tr>
                <tr>
                    <td ><label>Email</label></td>
                    <td><input type="text" name='email'/></td>
                </tr>
                <tr>
                    <td ></td>
                    <td><div class="button_holder"><button>SUBSCRIBE</button></div></td>
                </tr>
            </table>
        </form>
        <div class="subscribe_box_footer"></div>
    </li>
</ul>