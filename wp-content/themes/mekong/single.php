<?php get_header(); ?>
<?php
if (wpmd_is_phone()) {
    include_once 'mobile/single.php';
    get_footer();
    return;
}
?>
<script type="text/javascript">
    (function() {
        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
        po.src = 'https://apis.google.com/js/plusone.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
    })();
</script>
<?php while (have_posts()) : the_post(); ?>
    <div class="main-page-new">
        <div class="row">
            <div class="large-1 columns end">&nbsp;</div>
            <div class="large-18 columns end news-list">
                <div class="large-13 small-20 columns end">
                    <div class="tab">
                        <?php
                        $categories = get_the_category();
                        $currentCat = $categories[0]->cat_ID;
                        //echo "cat is ".$currentCat;
                        ?>
                        <?php if (ICL_LANGUAGE_CODE == 'vi'): ?>
                            <a href="<?php echo get_page_link(32) . '?lang=vi'; ?>"><li class="new-all large-5 small-7 columns end">Tất cả </li></a>
                            <a href="<?php echo get_category_link(3); ?>"><li class="new-only large-5 small-7 columns end  <?php
                    if ($currentCat == 16): echo "active";
                    endif;
                            ?>">Tin tức</li></a>
                            <a href="<?php echo get_category_link(4); ?>"><li class="new-photo large-5 small-6 columns end  <?php
                                                                      if ($currentCat == 17): echo "active";
                                                                      endif;
                            ?>">Hình ảnh & Video</li></a>
                            <?php else: ?>

                            <a href="<?php echo get_page_link(32); ?>"><li class="new-all large-5 small-7 columns end ">All</li></a>
                            <a href="<?php echo get_category_link(3); ?>"><li class="new-only large-5 small-6 columns end <?php
                        if ($currentCat == 3): echo "active";
                        endif;
                                ?>">News</li></a>
                            <a href="<?php echo get_category_link(4); ?>"><li class="new-photo large-7 small-6 columns end <?php
                                                                      if ($currentCat == 4): echo "active";
                                                                      endif;
                                ?>">Photos & Videos</li></a>
                            <?php endif; ?>
                    </div>
                    <div class="large-20 small-20 columns end">
                        <h3><a href="#"><?php the_title(); ?></a></h3>
                    </div>
                    <div class="large-20 columns end content-new-detail" style="margin-top: 20px;">
                        <?php the_content(); ?>
                    </div>
                    <div class="socail small-20 columns end">
                        <a class="addthis_button_facebook_like left" fb:like:layout="button_count"></a>
                        <a class="addthis_button_tweet left" tw:via="addthis"></a>
                        <a class="addthis_button_google_plusone left" g:plusone:size="medium"></a>
                        <a class="addthis_counter addthis_pill_style left" style="width: 50px;"></a>
                        <div class="clear"></div>
                        <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-50165fd7193ceae4"></script>
                    </div>
                    <hr />
                    <div class="other-new">
                        <?php
                        global $post;
                        $categories = get_the_category($post->ID);
                        //print_r($categories[0]->term_id);
                        ?>
                        <?php
                        $args = array(
                            'cat' => $categories[0]->term_id,
                            'posts_per_page' => 7,
                            'post_type' => 'post',
                            'post_status' => 'publish'
                        );

                        query_posts($args);
                        ?>
                        <h3>Other news</h3>
                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                                <li>
                                    <span><?php echo get_the_time('d/m/Y', $post->ID); ?> : </span><br />
                                    <a href="<?php echo the_permalink() ?>"><?php the_title(); ?></a>
                                </li>
                                <?php
                            endwhile;
                        endif;
                        wp_reset_query();
                        ?>
                    </div>
                </div>

                <!--            <div class="large-1 columns end">&nbsp;</div>-->
                <div class="large-6 columns right hide-for-small">
                    <div class="feature">
                        <h2>Features</h2>
                        <hr />
                        <?php
                        query_posts('post_type=post&posts_per_page=10&orderby=post_date&order=DESC&post_status=publish&featured=yes');

                        if (have_posts()) : while (have_posts()) : the_post();
                                ?>
                                <div class="box-thumbnail-new">
                                    <div class="large-7 thumbnail-image left">
                                        <a href="<?php echo the_permalink(); ?>">
                                            <?php $images = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'thumbnail'); ?>
                                            <img style="float: left;" src="<?php echo $images[0]; ?>" alt="" />
                                        </a>
                                    </div>
                                    <div class="large-13 columns title-feature-new left">
                                        <p><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></p>
                                        <p class="post-date"><?php echo get_the_time('M d, Y', $post->ID); ?></p>
                                    </div>
                                </div>
                                <?php
                            endwhile;
                        endif;
                        wp_reset_query();
                        ?>
                    </div>
                </div>
            </div>
            <div class="large-1 columns end">&nbsp;</div>
        </div>
    </div>
<?php endwhile; // end of the loop.     ?>
<?php get_footer(); ?>