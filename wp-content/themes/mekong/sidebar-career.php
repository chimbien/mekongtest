<div class="sidebar-left">
    <?php $page_default = get_field('page_default'); ?>
    <ul>
        <?php
        $args = array(
            'sort_order' => 'ASC',
            'sort_column' => 'menu_order, post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 151,
            'parent' => 151,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish'
        );
        $staff = get_pages($args);
        foreach ($staff as $s):
            ?>
            <li <?php
        if (($page_default->ID == $s->ID) || ($s->ID == $post->ID) || ($s->ID == $post->post_parent)) {
            echo 'class="active"';
        }
            ?>>
                <a href="<?php echo get_page_link($s->ID); ?>"><?php echo $s->post_title; ?></a>
               
            </li> 
            <li class="divider"></li>
            <?php
        endforeach;
        ?>
    </ul>
</div>