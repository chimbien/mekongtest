<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>
<?php
if (wpmd_is_phone()) {
  include_once 'mobile/content.php';
  return;
}
?>
<div class="box-new">
    <div class="large-20 small-20 columns end">
        <?php if (is_single()) : ?>
            <h3><?php the_title(); ?></h3>
        <?php else : ?>
            <h3>
                <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
            </h3>
        <?php endif; // is_single() ?>        
    </div>

    <?php if (has_post_thumbnail() && !post_password_required()) : ?>
        <div class="large-8 small-20 columns end thumb-new">
            <?php the_post_thumbnail('full'); ?>
        </div>
    <?php endif; ?>
    <div class="large-12 small-20 columns end">
        <?php the_content('Read more');  ?>
<p class="date-published"><?php echo get_the_date( 'g:i a F j Y' ); ?></p>
    </div>
</div>
<div class="large-20 columns end"><hr /></div>
