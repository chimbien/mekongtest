<?php
/**
 * The template for displaying posts in the Gallery post format.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>
<?php
if (wpmd_is_phone()) {
  include_once 'mobile/content-gallery.php';
  return;
}
?>
<div class="box-new">
    <div class="large-20 small-20 columns end">
        <?php if (is_single()) : ?>
            <h3><?php the_title(); ?></h3>
        <?php else : ?>
            <h3>
                <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
            </h3>
        <?php endif; // is_single() ?>
    </div>

    <div class="large-20 small-20 columns end thumb-new" style="">       
        <?php the_content('View more <span class="date-published" style="margin-left: 20px;">'.get_the_date( 'g:i a F j Y' ).'</span>'); ?>
    </div>
    <!--<div class="large-20 small-20 columns end">
        <p>December 12, 2012. MEKONG holds the closing ceremony of the 5th computer training course at its mobile school project Vinh Long province in end of May 2012. So far a total of 1,500 participants attend the training courses offered by the Henela's Mobile School project funded...... <a class="hide-for-small" href="#">Read more</a></p>

    </div>-->
</div>
<div class="large-20 columns end"><hr /></div>