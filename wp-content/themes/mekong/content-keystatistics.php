<div class="involve2" id="IE8-involve2">
    <div class="row">
        <div class="large-1 columns end ">&nbsp;</div>
        <div class="large-18 small-20 columns end">            
            <h2><?php the_title(); ?></h2>
            <?php
            $defaults = array(
                'post_parent' => $post->ID,
                'orderby' => 'menu_order',
                'order' => 'ASC',
                'post_type' => 'page',
                'numberposts' => -1,
                'post_status' => 'publish'
            );
            $page_parent = get_children($defaults);
            ?>
            <?php foreach ($page_parent as $re): ?> 
               
                <div class="large-10 small-20 columns end box-involved">
                    <?php if (get_post_meta($re->ID, 'icon', true)): ?>
                        <div class="large-5 small-8 columns end icon-involved">
                            <?php echo wp_get_attachment_image(get_post_meta($re->ID, 'icon', true), 'full'); ?>
                            <!--<img src="<?php echo get_post_meta($re->ID, 'icon', true); ?>" alt="" />-->
                        </div>
                        <div class="large-15 small-12 columns end">
                            <h3 <?php if (get_post_meta($re->ID, 'money_viet_nam', true)): ?> class="title-vn"<?php endif; ?>><?php echo $re->post_title; ?></h3>
                            <?php if (get_post_meta($re->ID, 'money_viet_nam', true)): ?>
                            <h4 class="money-vietnam"><?php echo get_post_meta($re->ID, 'money_viet_nam', true); ?></h4>
                            <?php endif; ?>
                            <div class="">
                                <?php $content = apply_filters('the_content', $re->post_content); ?>
                                <?php echo $content; ?>
                            </div>
                        </div>
                        <div class="small-20 columns end show-for-small content-involved-mobile">
                            <?php $content = apply_filters('the_content', $re->post_content); ?>
                            <?php echo $content; ?>
                        </div>
                    <?php else: ?>
                        <?php $content = apply_filters('the_content', $re->post_content); ?>
                        <?php echo $content; ?>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>     

        </div>
        <div class="large-1 columns end ">&nbsp;</div>

    </div>
</div>