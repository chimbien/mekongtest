<?php

//add_theme_support( 'automatic-feed-links' );
add_theme_support('post-formats', array('gallery', 'video'));
if (function_exists('add_theme_support'))
    add_theme_support('post-thumbnails');
if (function_exists('wp_nav_menu')) {

    function wp_my_menus() {
        //đăng ký menu cho theme
        //register_nav_menus(array('primary-menu' => __('Menu top')));
        register_nav_menus(array(
            'primary' => __('Primary Navigation', 'Main menu'),
            'secondary' => __('Secondary Navigation', 'Menu about')
        ));
    }

    //theme vào cấu hình mặc định
    add_action('init', 'wp_my_menus');
}

function wp_nav_home_link($items) {
    if (is_home())
        $current = 'current-menu-item';
    $home .= '<li class="' . $current . ' item0" style="margin-left: 20px;"><a class="item0" href="' . home_url() . '">' . __('Home') . '</a></li>';
    //$home.= '<li class="divider">';
    $items = $home . $items;
    return $items;
}
add_filter('wp_nav_menu_items', 'wp_nav_home_link');
function wp_nav_about_link($items) {
    if (is_home())
        $current = 'current-menu-item';
/*    $home .= '<li class="' . $current . ' item1">' . __('Explore More') . '</li>';*/
    $home.= '<li class="divider item1">';
    $items = $home . $items;
    return $items;
}

add_filter('wp_nav_menu_items', 'wp_nav_about_link');

function get_images_from_media_library() {
    $args = array(
        'post_type' => 'attachment',
        'post_mime_type' => 'image',
        'post_status' => 'inherit',
        'posts_per_page' => 5,
        'orderby' => 'rand'
    );
    $query_images = new WP_Query($args);

    $images = array();
    foreach ($query_images->posts as $image) {
        $images[] = $image->guid;
    }
    return $images;
}

function display_images_from_media_library() {

    $imgs = get_images_from_media_library();
    $html = '<div id="media-gallery">';

    foreach ($imgs as $img) {

        $html .= '<img src="' . $img . '" alt="" />';
    }

    $html .= '</div>';

    return $html;
}

function new_excerpt_more($more) {
    return ' <a class="read-more" href="' . get_permalink(get_the_ID()) . '">Read more</a>';
}

add_filter('excerpt_more', 'new_excerpt_more');
/*if (function_exists('get_post_format_strings')) {

    //delete the function that is decleared in includes/post.php
    //declear new 'get_post_format_strings' function :
    function get_post_format_strings() {
        $strings = array(
            'standard' => _x('Standard', 'Post format'), // Special case. any value that evals to false will be considered standard
            'aside' => _x('Aside', 'Post format'),
            'chat' => _x('Chat', 'Post format'),
            'gallery' => _x('Gallery', 'Post format'),
            'link' => _x('Link', 'Post format'),
            'image' => _x('Image', 'Post format'),
            'quote' => _x('Quote', 'Post format'),
            'status' => _x('Status', 'Post format'),
            'video' => _x('Video', 'Post format'),
            'audio' => _x('Audio', 'Post format'),
            'product' => _x('Product', 'Post format'),
        );
        return $strings;
    }

}*/
add_shortcode('gallery', 'my_gallery_shortcode');

function my_gallery_shortcode($attr) {
    $post = get_post();

    static $instance = 0;
    $instance++;

    if (!empty($attr['ids'])) {
        // 'ids' is explicitly ordered, unless you specify otherwise.
        if (empty($attr['orderby']))
            $attr['orderby'] = 'post__in';
        $attr['include'] = $attr['ids'];
    }

// Allow plugins/themes to override the default gallery template.
    $output = apply_filters('post_gallery', '', $attr);
    if ($output != '')
        return $output;

// We're trusting author input, so let's at least make sure it looks like a valid orderby statement
    if (isset($attr['orderby'])) {
        $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
        if (!$attr['orderby'])
            unset($attr['orderby']);
    }

    extract(shortcode_atts(array(
                'order' => 'ASC',
                'orderby' => 'menu_order ID',
                'id' => $post->ID,
                'itemtag' => 'dl',
                'icontag' => 'dt',
                'captiontag' => 'dd',
                'columns' => 3,
                'size' => 'thumbnail',
                'include' => '',
                'exclude' => ''
                    ), $attr));

    $id = intval($id);
    if ('RAND' == $order)
        $orderby = 'none';

    if (!empty($include)) {
        $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

        $attachments = array();
        foreach ($_attachments as $key => $val) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    } elseif (!empty($exclude)) {
        $attachments = get_children(array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));
    } else {
        $attachments = get_children(array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));
    }

    if (empty($attachments))
        return '';

    if (is_feed()) {
        $output = "\n";
        foreach ($attachments as $att_id => $attachment)
            $output .= wp_get_attachment_link($att_id, $size, true) . "\n";
        return $output;
    }

    $itemtag = tag_escape($itemtag);
    $captiontag = tag_escape($captiontag);
    $icontag = tag_escape($icontag);
    $valid_tags = wp_kses_allowed_html('post');
    if (!isset($valid_tags[$itemtag]))
        $itemtag = 'dl';
    if (!isset($valid_tags[$captiontag]))
        $captiontag = 'dd';
    if (!isset($valid_tags[$icontag]))
        $icontag = 'dt';

    $columns = intval($columns);
    $itemwidth = $columns > 0 ? floor(100 / $columns) : 100;
    $float = is_rtl() ? 'right' : 'left';

    $selector = "gallery-{$instance}";

    $gallery_style = $gallery_div = '';
    if (apply_filters('use_default_gallery_style', true))
        $gallery_style = "
    <style type='text/css'>
        #{$selector} {
            margin: auto;
        }
        #{$selector} .gallery-item {
            float: {$float};
            margin-top: 10px;
            text-align: center;
            width: {$itemwidth}%;
        }
        #{$selector} img {
            border: 2px solid #cfcfcf;
        }
        #{$selector} .gallery-caption {
            margin-left: 0;
        }
    </style>
    <!-- see gallery_shortcode() in wp-includes/media.php -->";
    $size_class = sanitize_html_class($size);
    $gallery_div = "<div id='$selector' class='gallery galleryid-{$id} gallery-columns-{$columns} gallery-size-{$size_class}'>";
    $output = apply_filters('gallery_style', $gallery_style . "\n\t\t" . $gallery_div);

    $i = 0;
    foreach ($attachments as $id => $attachment) {
        $link = isset($attr['link']) && 'file' == $attr['link'] ? wp_get_attachment_link($id, $size, false, false) : wp_get_attachment_link($id, $size, true, false);

        $output .= "<{$itemtag} class='gallery-item'>";
        $output .= "
        <{$icontag} class='gallery-icon'>
            $link
        </{$icontag}>";
        if ($captiontag && trim($attachment->post_excerpt)) {
            $output .= "
            <{$captiontag} class='wp-caption-text gallery-caption'>
            " . wptexturize($attachment->post_excerpt) . "
            </{$captiontag}>";
        }
        $output .= "</{$itemtag}>";
        if ($columns > 0 && ++$i % $columns == 0)
            $output .= '<br style="clear: both" />';
    }

    $output .= "
        <br style='clear: both;' />
    </div>\n";

    return $output;
}
?>
