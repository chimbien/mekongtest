<?php
/*
  Template Name: Template News & Media
 */
?>
<?php get_header(); ?>
<?php
if (wpmd_is_phone()) {
    include_once 'mobile/page-news.php';
    get_footer();
    return;
}
?>
<?php while (have_posts()) : the_post(); ?>
    <div class="main-page-new">
        <div class="row">
            <div class="large-1 columns end">&nbsp;</div>
            <div class="large-18 columns end news-list">
                <div class="large-13 small-20 columns end page-new">
                    <div class="tab">
						<?php if (ICL_LANGUAGE_CODE == 'vi'): ?>
                            <a href="<?php echo get_page_link(32) . '?lang=vi'; ?>"><li class="new-all large-5 small-7 columns end active">Tất cả </li></a>
                            <a href="<?php echo get_category_link(3); ?>"><li class="new-only large-5 small-7 columns end">Tin tức</li></a>
                       		<a href="<?php echo  get_category_link(4); ?>"><li class="new-photo large-5 small-6 columns end">Media</li></a>
                        <?php else: ?>
                             <a href="<?php echo get_page_link(32) . '?lang=vi'; ?>"><li class="new-all large-5 small-7 columns end active">All</li></a>
                            <a href="<?php echo get_category_link(3); ?>"><li class="new-only large-5 small-6 columns end">News</li></a>
                       		<a href="<?php echo  get_category_link(4); ?>"><li class="new-photo large-5 small-6 columns end">Media</li></a>
                        <?php endif; ?>
                        
                    </div>
                    <?php
                    $args = array(
                        'cat' => '3,4',
                        'posts_per_page' => 5,
                        'post_status' => 'publish'
                    );
                    global $more;    // Declare global $more (before the loop).
                    $more = 0;
                    query_posts($args);
                    if (have_posts()) : while (have_posts()) : the_post();
                            get_template_part('content', get_post_format());
                        endwhile;
                    endif;
                    wp_reset_query();
                    ?>

                    <!--<div class="large-20 small-20 columns end">
                        <div class="paging">
                            <div class="pre large-5 small-7 left"></div>
                            <div class="paging-list large-10 small-7 left">1/20</div>
                            <div class="next large-5 small-6 right"></div>
                        </div>
                    </div>-->
                </div>
                <!--            <div class="large-1 columns end">&nbsp;</div>-->
                <div class="large-6 columns right hide-for-small">
                    <div class="feature">
                        <h2><?php if (ICL_LANGUAGE_CODE == 'vi'){ echo 'Tin nổi bật';}else{ echo 'Features';} ?></h2>
                        <hr />
                        <?php
                        query_posts('post_type=post&posts_per_page=10&orderby=post_date&order=DESC&post_status=publish&featured=yes');

                        if (have_posts()) : while (have_posts()) : the_post();
                                ?>
                                <div class="box-thumbnail-new">
                                    <div class="large-7 thumbnail-image left">
                                        <a href="<?php echo the_permalink(); ?>">
                                            <?php $images = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'thumbnail'); ?>
                                            <img style="float: left;" src="<?php echo $images[0]; ?>" alt="" />
                                        </a>
                                    </div>
                                    <div class="large-13 columns title-feature-new left">
                                        <p><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></p>
                                        <p class="post-date"><?php echo get_the_time('M d, Y',$post->ID);?></p>
                                    </div>
                                </div>
                                <?php
                            endwhile;
                        endif;
                        wp_reset_query();
                        ?>
                    </div>
                </div>
            </div>
            <div class="large-1 columns end">&nbsp;</div>
        </div>
    </div>
<?php endwhile; // end of the loop.  ?>
<?php get_footer(); ?>

<script>
$("#menu-item-236").addClass("current-menu-item");
</script>