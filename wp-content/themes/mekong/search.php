<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
get_header();
?>
<?php if (have_posts()) : ?>
    <div class="main-content">
        <div class="row">
            <div class="large-1 columns end">&nbsp;</div>            
            <div class="large-18 columns end">
                <div class="page-about-team">
                    <h1 class="page-title"><?php printf(__('Search Results for: %s', 'twentyeleven'), '<span>' . get_search_query() . '</span>'); ?></h1>
                    <?php while (have_posts()) : the_post(); ?>
                        <div class="box-search">
                            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            <?php the_excerpt(); ?>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
            <div class="large-1 columns end">&nbsp;</div>
        </div>
    </div>
<?php else : ?>
    <div class="main-content">
        <div class="row">
            <div class="large-1 columns end">&nbsp;</div>            
            <div class="large-18 columns end">
                <div class="page-about-team">
                    <h3><?php _e('Nothing Found', 'twentyeleven'); ?></h3>
                    <p><p><?php _e('Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'twentyeleven'); ?></p></p>
                </div>
            </div>
            <div class="large-1 columns end">&nbsp;</div>
        </div>
    </div>
<?php endif; ?>    
<?php //get_sidebar(); ?>
<?php get_footer(); ?>