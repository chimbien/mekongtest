<?php
/*
  Template Name: Template About team
 */
?>
<?php get_header(); ?>
<?php
if (wpmd_is_phone()) {
  include_once 'mobile/page-about-team.php';
  get_footer();
  return;
}
?>
<?php while (have_posts()) : the_post(); ?>
    <div class="page-banner">
        <img src="<?php echo get_field('page_banner'); ?>" />
        <div class="row hide-for-small">
            <div class="large-9 small-20 page-decription">
                <?php echo get_field('introduction_page'); ?>
            </div>
        </div>
    </div>
    <div class="main-content">
        <div class="row">
            <div class="large-1 columns end hide-for-small">&nbsp;</div>
            <div class="large-18 columns end">
                <div class="page-about-team">
                    <div class="large-20 columns end hide-for-small">
                        <div class="title-page-boxmenu">
                            <div class="title-page large-15 left"><?php the_title(); ?></div>
                            <div class="boxmenu large-4 right">
                                <?php the_title(); ?>
                            </div>
                            <div class="icon-boxmenu"></div>
                            <div class="menu-box large-4">
                                <?php
                                $args = array();
                                $args = array(
                                    'theme_location' => 'secondary',
                                    'container' => FALSE,
                                    'menu' => 'Menu about',
                                    'menu_class' => '',
                                    'echo' => true,
                                    'fallback_cb' => '',
                                    'depth' => 4,
                                    //'after' => '<li class="divider"></li>',                        
                                    'menu_id' => '');
                                wp_nav_menu($args);
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="page-content-about-team">
                        <?php
                        $defaults = array(
                            'post_parent' => $post->ID,
                            'orderby' => 'menu_order',
                            'order' => 'ASC',
                            'post_type' => 'page',
                            'numberposts' => -1,
                            'post_status' => 'publish'
                        );
                        $page_parent = get_children($defaults);
                        ?>
                        <?php foreach ($page_parent as $index => $re): ?>                          

                            <div class="box-team">
                                <div class="large-4 small-20 columns end">
                                    <div class="thumbnail-team">
                                        <?php $images = wp_get_attachment_image_src(get_post_thumbnail_id($re->ID), 'full'); ?>
                                        <img src="<?php echo $images[0]; ?>"  alt="" />
                                    </div>
                                    <div class="name-team"><?php echo $re->post_title; ?></div>
                                    <div class="position-team small-20 show-for-small columns end"><?php echo get_post_meta($re->ID, 'position', true); ?></div>
                                </div>
                                <div class="large-16 small-20 columns end">
                                    <div class="content-team-detail">
                                        <h3 class="hide-for-small"><?php echo get_post_meta($re->ID, 'position', true); ?></h3>
                                        <?php $content = apply_filters('the_content', $re->post_content); ?>
                                        <?php echo $content; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>

                    </div>
                </div>
            </div>
            <div class="large-1 columns end">&nbsp;</div>
        </div>
    </div>
<?php endwhile; // end of the loop.  ?>
<?php get_footer(); ?>

<script>
$("#menu-item-234").addClass("current-menu-item");
</script>