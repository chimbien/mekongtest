<?php
/*
  Template Name: Template Contact
 */
?>
<?php get_header(); ?>
<?php
if (wpmd_is_phone()) {
  include_once 'mobile/page-contact.php';
  get_footer();
  return;
}
?>
<?php while (have_posts()) : the_post(); ?>
    <div class="page-banner">
        <div id="map_canvas" style="width:100%; height:350px"></div>
        <!--<img src="<?php echo get_field('page_banner'); ?>" alt="" />  -->
<?php //echo get_post_meta($post->ID, 'google_map', true); ?>
    </div>
    <div class="main-page-contact">
        <div class="row">
            <div class="large-1 columns end ">&nbsp;</div>
            <div class="large-18 small-20 columns end">
                <h2><?php if (ICL_LANGUAGE_CODE == 'vi'){ echo 'Hãy liên lạc với chúng tôi';}else{ echo 'get in touch with us';} ?></h2>
                <div class="large-6 small-20 columns end">
                    <div class="box-address">
                        <h3><?php echo get_post_meta($post->ID, 'company_name', true); ?></h3>
                        <p><?php echo get_post_meta($post->ID, 'address', true); ?></p>
                        <p>
                        <span><img src="<?php bloginfo('template_url'); ?>/images/icon_phone.png" alt="" />  <?php echo get_post_meta($post->ID, 'phone', true); ?></span>
                        <span style="float:right;"><img src="<?php bloginfo('template_url'); ?>/images/icon_email.png" alt="" />  <?php echo get_post_meta($post->ID, 'email', true); ?></span></p>
                        <p>
                        <span><img src="<?php bloginfo('template_url'); ?>/images/icon_fax.png" alt="" />  <?php echo get_post_meta($post->ID, 'fax', true); ?>
                        </span>
                        <span style="float:right;"><img src="<?php bloginfo('template_url'); ?>/images/icon_website.png" alt="" />  <?php echo get_post_meta($post->ID, 'website', true); ?></span></p>
                    </div>
                    <br style="clear:both"/>
                    <div class="face-book">
                        <!--<img src="<?php bloginfo('template_url'); ?>/images/face_book.jpg" />-->
<?php echo get_field('facebook_like'); ?> 
                    </div>
                    <div class="facebook-twitter" style="display:none">
                        <a href="<?php echo get_post_meta($post->ID, 'link_facebook', true); ?>"><img src="<?php bloginfo('template_url'); ?>/images/facebook_c.png" /></a>
                        <a href="<?php echo get_post_meta($post->ID, 'link_twitter', true); ?>"><img src="<?php bloginfo('template_url'); ?>/images/twitter_c.jpg" /></a>
                        <a href="<?php echo get_post_meta($post->ID, 'link_google', true); ?>"><img src="<?php bloginfo('template_url'); ?>/images/google_c.jpg" /></a>
                    </div>
                </div>
                <?php the_content(); ?>
            </div>

            <div class="large-1 columns end">&nbsp;</div>
        </div>
    </div>
<?php endwhile; // end of the loop.  ?>
<?php get_footer(); ?>