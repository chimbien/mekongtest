<?php get_header(); ?>
<div class="page-banner">
    <img src="<?php bloginfo('template_url'); ?>/images/banner_about.jpg" />
</div>
<?php while (have_posts()) : the_post(); ?>
    <div class="main-content">
        <div class="row">
            <div class="large-1 columns end">&nbsp;</div>            
            <div class="large-18 columns end">
                <div class="page-about-team">
                    <div class="large-20 columns end">
                        <div class="title-page-boxmenu">
                            <div class="title-page large-15 left"><?php the_title(); ?></div>
                        </div>
                    </div>
                    <div class="page-about-content">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
            <div class="large-1 columns end">&nbsp;</div>
        </div>
    </div>
<?php endwhile; // end of the loop.   ?>
<?php get_footer(); ?>

