//////////

<?php get_header(); ?>
<?
if (wpmd_is_phone()) {
  include_once 'mobile/index.php';
  get_footer();
  return;
}
?>
<style>
.footer{
	display:none;
}

/*
.header {
    background: none repeat scroll 0 0 #FAFAFA;
    float: left;
    height: 85px;
    margin-top: -0px;
    overflow: hidden;
    width: 100%;
}
*/
#thumb-tray {
    bottom: 0px !important;
    height: 167px;
    left: 0;
    overflow: hidden;
    padding: 20px 30px 20px 20px;
    position: fixed;
    text-align: center;
    width: 100%;
    z-index: 3;
}

</style>

<div class="hide-for-small">
    <!-- Slide Home -->
   <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/lib/slideshow/css/supersized.css" type="text/css" media="screen" />
     <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/lib/slideshow/theme/supersized.shutter.css" type="text/css" media="screen" />
 
     <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/lib/slideshow/js/jquery.easing.min.js"></script>
 
     <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/lib/slideshow/js/supersized.3.2.7.js"></script>
     <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/lib/slideshow/theme/supersized.shutter.min.js"></script>
    <!-- End Slide Home -->
    <script type="text/javascript">
                        
        jQuery(function($){
                                
            $.supersized({
                                
                // Functionality
                slideshow               :   1,			// Slideshow on/off
                autoplay				:	0,			// Slideshow starts playing automatically
                start_slide             :   1,			// Start slide (0 is random)
                stop_loop				:	0,			// Pauses slideshow on last slide
                random					: 	0,			// Randomize slide order (Ignores start slide)
                slide_interval          :   3000,		// Length between transitions
                transition              :   6, 			// 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
                transition_speed		:	1000,		// Speed of transition
                new_window				:	0,			// Image links open in new window/tab
                pause_hover             :   1,			// Pause slideshow on hover
                keyboard_nav            :   1,			// Keyboard navigation on/off
                performance				:	3,			// 0-Normal, 1-Hybrid speed/quality, 2-Optimizes image quality, 3-Optimizes transition speed // (Only works for Firefox/IE, not Webkit)
                image_protect			:	1,			// Disables image dragging and right click with Javascript
                                                                                                                           
                // Size & Position						   
                min_width		        :   0,			// Min width allowed (in pixels)
                min_height		        :   0,			// Min height allowed (in pixels)
                vertical_center         :   1,			// Vertically center background
                horizontal_center       :   1,			// Horizontally center background
                fit_always				:	0,			// Image will never exceed browser width or height (Ignores min. dimensions)
                fit_portrait         	:   1,			// Portrait images will not exceed browser height
                fit_landscape			:   1,			// Landscape images will not exceed browser width
                                                                                                                           
                // Components							
                slide_links				:	'blank',	// Individual links for each slide (Options: false, 'num', 'name', 'blank')
                thumb_links				:	0,			// Individual thumb links for each slide
                thumbnail_navigation    :   0,			// Thumbnail navigation
                slides 					:  	[			// Slideshow Images
                    {image : '<?php bloginfo('template_url'); ?>/images/slide1.jpg', title : 'Microfinance',showContent : 1, contentTitle : 'Microfinance',contentHeadline: 'Bring Opportunities to Mekong Community',contentLink1:"#",contentLink2:"#",thumb : '<h3>Microfinance</h3><p>Lorem Ipsum. Proin gravida nibh vel velit auctor acfgjyre aliqAenean Aandw sollicitudin.</p>', url : '#'},
					{image : '<?php bloginfo('template_url'); ?>/images/slide3.jpg', title : 'Mobile School',showContent : 1, contentTitle : 'Mobile School',contentHeadline: 'Education For Unadvangate Kids',contentLink1:"#",contentLink2:"#",thumb : '<h3>Mobile School</h3><p>Lorem Ipsum. Proin gravida nibh vel velit auctor acfgjyre aliqAenean Aandw sollicitudin.</p>', url : '#'},
					{image : '<?php bloginfo('template_url'); ?>/images/slide3.jpg', title : 'Scholarship',showContent : 1, contentTitle : 'Scholarship',contentHeadline: 'Scholarship For Unadvangate Kids',contentLink1:"#",contentLink2:"#",thumb : '<h3>Scholarship</h3><p>Lorem Ipsum. Proin gravida nibh vel velit auctor acfgjyre aliqAenean Aandw sollicitudin.</p>', url : '#'},
					{image : '<?php bloginfo('template_url'); ?>/images/slide3.jpg', title : 'School Building',showContent : 1, contentTitle : 'School Building',contentHeadline: 'School Building For Unadvangate Kids',contentLink1:"#",contentLink2:"#",thumb : '<h3>School Building</h3><p>Lorem Ipsum. Proin gravida nibh vel velit auctor acfgjyre aliqAenean Aandw sollicitudin.</p>', url : '#'},
					{image : '<?php bloginfo('template_url'); ?>/images/slide3.jpg', title : 'Financial Education',showContent : 1, contentTitle : 'Financial Education',contentHeadline: 'Financial Education for Unadvangate Kids',contentLink1:"#",contentLink2:"#",thumb : '<h3>Financial Education</h3><p>Lorem Ipsum. Proin gravida nibh vel velit auctor acfgjyre aliqAenean Aandw sollicitudin.</p>', url : '#'},
					{image : '<?php bloginfo('template_url'); ?>/images/slide3.jpg', title : 'Mobile Dental Clinics',showContent : 1, contentTitle : 'Mobile Dental Clinics',contentHeadline: 'Mobile Dental Clinic  For Unadvangate Kids',contentLink1:"#",contentLink2:"#",thumb : '<h3>Mobile Dental Clinics</h3><p>Lorem Ipsum. Proin gravida nibh vel velit auctor acfgjyre aliqAenean Aandw sollicitudin.</p>', url : '#'},
					{image : '<?php bloginfo('template_url'); ?>/images/slide3.jpg', title : 'Mobile Vocational Center',showContent : 1, contentTitle : 'Mobile Vocational Center',contentHeadline: 'Vocational Center For Unadvangate Kids',contentLink1:"#",contentLink2:"#",thumb : '<h3>Mobile Vocational Center</h3><p>Lorem Ipsum. Proin gravida nibh vel velit auctor acfgjyre aliqAenean Aandw sollicitudin.</p>', url : '#'}
                ],
                                                                                                
                // Theme Options			   
                progress_bar			:	0,			// Timer for each slide							
                mouse_scrub				:	0
                                        
            });
			
        });
	  });  
	  
	   $(document).ready(function(){
		   alert("a");
		  $("#supersized li a").click(function(event){
			event.preventDefault();
		});
		
		    
    </script>
    ==
    <div class="slide-home">
       
        <div id="thumb-tray" class="load-item" style="display: block;">
        	<div id="thumb-back-bg"></div>
            <div id="thumb-forward-bg"></div>
            <div id="thumb-back"></div>
            <div id="thumb-forward"></div>
        </div>


        <!--Control Bar-->
        <div id="controls-wrapper" class="load-item">
            <div id="controls">

                <!--Navigation-->
                <ul id="slide-list"></ul>

            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>