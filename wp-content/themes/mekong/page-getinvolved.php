<?php
/*
  Template Name: Template Get Involved
 */
?>
<?php get_header(); ?>
<?php
if (wpmd_is_phone()) {
    include_once 'mobile/page-getinvolved.php';
    get_footer();
    return;
}
?>
<script type="text/javascript">
		$(document).ready(function(){
			Cufon.replace('.box-involved h3', { fontFamily: 'giorgiosans-cufont'});
		});
	</script>
<?php while (have_posts()) : the_post(); ?>
    <div class="banner-involved">
        <img src="<?php echo get_field('page_banner'); ?>" />
        <div class="row ">        
            <div class="large-20 columns end">                
                <h2><?php the_title(); ?></h2>
                <?php if (ICL_LANGUAGE_CODE == 'vi'){
                    $language_text = '?lang=vi';
                }?>
                <a href="<?php echo get_page_link(134).$language_text; ?>" class="large-4 small-10 columns end btn_vol1">
                    <h3>Volunteer</h3>
                    <p>or work for us</p>
                </a>
<?php
                $donate = get_page(137);
                ?>
                <a href="<?php echo get_page_link(137).$language_text; ?>" class="large-4 small-10 columns end btn_vol2">
                    <h3>Donate</h3>
                    <p>or become a supporter</p>
                </a>
            </div>
        </div>
    </div>
    <?php
    global $post;
    $args = array(
        'post_type' => 'page',
        'posts_per_page' => -1,
        'post_parent' => $post->ID,
        'order' => 'ASC',
        'orderby' => 'menu_order'
    );       // Declare global $more (before the loop).

    query_posts($args);
    if (have_posts()) : while (have_posts()) : the_post();
            if ((get_post_meta($post->ID, 'position_box', true) == 1) || (get_post_meta($post->ID, 'position_box', true) == 3)) {
                get_template_part('content', 'getinvolced');
            } else {
                get_template_part('content', 'keystatistics');
            }
        endwhile;
    endif;
    wp_reset_query();
    ?>
    <div class="link-involved ">
        <div class="row">
            <div class="large-5 columns end">&nbsp;</div>
            <div class="large-12 columns end btn-link-vol">
                
                <a href="<?php echo get_page_link(134).$language_text; ?>" class="large-6 small-10 columns end btn_vol1_small">
                    <?php
                    $volunteer = get_page(134);
                    echo $volunteer->post_title;
                    ?>
                </a>
                <span class="large-3 small-10 columns end" style="line-height: 85px;font-size: 31px;color: #cdcdcd;"><?php if (ICL_LANGUAGE_CODE == 'vi'){ echo 'Hoặc';}else{ echo 'OR';} ?></span>
                <?php
                $donate = get_page(137);
                ?>
                <a href="<?php echo get_page_link(137).$language_text; ?>" class="large-6 small-10 columns end btn_vol2_small">
                    <?php echo $donate->post_title; ?>                   
                </a>
            </div>
            <div class="large-3 columns end">&nbsp;</div>
        </div>
    </div>
<?php endwhile; // end of the loop.  ?>
<script>
$("#menu-item-237").addClass("current-menu-item");
 if (/MSIE\s([\d.]+)/.test(navigator.userAgent)) {
                //Get the IE version.  This will be 6 for IE6, 7 for IE7, etc...
                version = new Number(RegExp.$1);
                if(version == 8){ 
					
					$(document).ready(function(){
						var cnt=0;
						$("#IE8-involve2 div.icon-involved img").each(function(){
						//	alert("a");
							cnt++;
							$(this).attr("width",100);
							$(this).attr("height",74);
						});
						//alert(cnt);
					});
					
				}
 }
</script>

<?php get_footer(); ?>