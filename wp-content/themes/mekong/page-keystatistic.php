<?php
/*
  Template Name: Template Key Statistic
 */
?>
<?php get_header(); ?>
<?php
if (wpmd_is_phone()) {
    include_once 'mobile/page-keystatistic.php';
    get_footer();
    return;
}
?>

<!--[if !IE]><!-->
<script type="text/javascript">
    $(document).ready(function(){
        Cufon.replace('#number-gender-ie', { fontFamily: 'giorgiosans-cufont'});
        Cufon.replace('#number-school-ie', { fontFamily: 'giorgiosans-cufont'});
        Cufon.replace('#province-school-ie', { fontFamily: 'giorgiosans-cufont'});
        Cufon.replace('#student-ie', { fontFamily: 'giorgiosans-cufont'});
        Cufon.replace('.number-student h2 span', { fontFamily: 'giorgiosans-cufont'});
        Cufon.replace('#phantram_toilet-ie', { fontFamily: 'giorgiosans-cufont'});
        Cufon.replace('#info_toilet-ie', { fontFamily: 'giorgiosans-cufont'});
        if (/*@cc_on!@*/false) {
            Cufon.replace('#number-gender-ie', { fontFamily: 'giorgiosans-cufont'});
            Cufon.replace('#number-school-ie', { fontFamily: 'giorgiosans-cufont'});
            Cufon.replace('#province-school-ie', { fontFamily: 'giorgiosans-cufont'});
            Cufon.replace('#student-ie', { fontFamily: 'giorgiosans-cufont'});
            Cufon.replace('#number-student-span-ie10', { fontFamily: 'giorgiosans-cufont'});
            Cufon.replace('#phantram_toilet-ie', { fontFamily: 'giorgiosans-cufont'});
            Cufon.replace('#info_toilet-ie', { fontFamily: 'giorgiosans-cufont'});
            //alert("ie10");
        }	
			
    });
</script>
<!--<!endif]-->



<?php while (have_posts()) : the_post(); ?>
    <div class="page-banner">
        <img src="<?php echo get_field('page_banner'); ?>" />
        <div class="row hide-for-small">
            <div class="large-9 small-20 page-decription">
                <?php echo get_field('introduction_page'); ?>
            </div>
        </div>
    </div>
    <div class="main-content">
        <div class="row">
            <div class="main-page-key-statistic">
                <div class="large-1 columns end ">&nbsp;</div>
                <div class="large-18 small-20 columns end">
                    <div class="large-20 columns end ">
                        <div class="title-page-boxmenu">
                            <div class="title-page large-15 left ie-title"><?php the_title(); ?></div>
                            <div class="boxmenu large-4 right">
                                <?php the_title(); ?>
                            </div>
                            <div class="icon-boxmenu"></div>
                            <div class="menu-box large-4">
                                <?php
                                $args = array();
                                $args = array(
                                    'theme_location' => 'secondary',
                                    'container' => FALSE,
                                    'menu' => 'Menu about',
                                    'menu_class' => '',
                                    'echo' => true,
                                    'fallback_cb' => '',
                                    'depth' => 4,
                                    //'after' => '<li class="divider"></li>',                        
                                    'menu_id' => '');
                                wp_nav_menu($args);
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="page-key-statistic">
                        <div class="title-page-key large-8 ">
                            <div class="bg-left"></div>
                            <div class="bg-center">FACTS & FIGURES</div>
                            <div class="bg-right"></div>
                        </div>
                        <div class="gender">
                            <div class="image-gender large-8 large-offset-1 columns end">
                                <img src="<?php bloginfo('template_url'); ?>/images/women.png" alt="" />
                                <img src="<?php bloginfo('template_url'); ?>/images/women.png" alt="" />
                                <img src="<?php bloginfo('template_url'); ?>/images/men.png" alt="" />
                                <img src="<?php bloginfo('template_url'); ?>/images/men.png" alt="" />
                            </div>
                            <div class="large-1 columns end">&nbsp;</div>
                            <div class="number-gender large-9 columns end">                            
                                <span class="number-gender-ie10"><h2 id="number-gender-ie">

                                        <?php echo get_post_meta($post->ID, 'gender_percent', true); ?>%

                                    </h2>
                                </span>
                                <p><?php echo get_post_meta($post->ID, 'gender_percent_text', true); ?></p>
                            </div>
                        </div>
                        <hr />
                        <div class="school">
                            <div class="">
                                <div class="image-school large-20 columns end">
                                    <?php for ($i = 0; $i < get_post_meta($post->ID, 'mobile_school', true); $i++): ?>
                                        <img src="<?php bloginfo('template_url'); ?>/images/icon_school.png" alt="" />
                                    <?php endfor; ?>
                                </div>
                                <div class="large-4 columns end">&nbsp;</div>
                                <div class="number-school large-7 columns end" id="IE8-number-school">                            
                                    <h2 id="number-school-ie">
                                        <?php
                                        if (get_post_meta($post->ID, 'mobile_school', true) < 10) {
                                            echo '0' . get_post_meta($post->ID, 'mobile_school', true);
                                        } else {
                                            echo get_post_meta($post->ID, 'mobile_school', true);
                                        }
                                        ?>
                                    </h2>

                                    <p><?php echo get_post_meta($post->ID, 'mobile_school_text', true); ?></p>
                                </div>
                                <div class="number-provinces large-5 columns end" id="IE8-number-provinces">                            
                                    <h2 id="province-school-ie">
                                        <?php
                                        if (get_post_meta($post->ID, 'provinces', true) < 10) {
                                            echo '0' . get_post_meta($post->ID, 'provinces', true);
                                        } else {
                                            echo get_post_meta($post->ID, 'provinces', true);
                                        }
                                        ?>
                                    </h2>
                                    <p><?php echo get_post_meta($post->ID, 'provinces_text', true); ?></p>
                                </div>
                            </div>

                        </div>
                        <hr />
                        <div class="student">                        
                            <div class="large-2 columns end  hide-for-medium">&nbsp;</div>
                            <div class="number-pass large-7 columns end" id="IE8-number-pass">                            
                                <h2 id="student-ie"><?php echo get_post_meta($post->ID, 'students_passed', true); ?>%</h2>
                                <p class=""><?php echo get_post_meta($post->ID, 'students_passed_text', true); ?></p>

                            </div>
                            <div class="large-2 columns end  hide-for-medium">&nbsp;</div>
                            <div class="number-student large-8 columns end" id="IE8-number-student">
                                <h2 class="number-student-mobile-h2" id="IE8-number-student-h2">
                                    <span>
                                        <?php echo get_post_meta($post->ID, 'students_joined', true); ?>
                                    </span>
                                    <p class="text-sutdent" id="number-student-p-ie10">students</p>
                                </h2>
                                <p class=""><?php echo get_post_meta($post->ID, 'students_joined_text', true); ?></p>
                            </div>
                        </div>
                        <hr />
                        <div class="toilet">                        
                            <div class="large-1 columns end ">&nbsp;</div>
                            <div class="toilet-left large-9 small-20 columns end">
                                <div class="large-5 small-7 columns end">                            
                                    <img id="IE8-toilet-left-img" src="<?php bloginfo('template_url'); ?>/images/icon_toilet.png" alt="" />
                                </div>
                                <div class="large-14 small-13 columns end">
                                    <?php $notolet = 100 - get_post_meta($post->ID, 'toilet_access', true); ?>
                                    <p class="ptop"><span class="phantram_toilet" id="phantram_toilet-ie"><?php echo get_post_meta($post->ID, 'toilet_access', true); ?>%</span> <span class="info-toilet" id="IE8-toilet-access"><?php echo get_post_meta($post->ID, 'toilet_access_text1', true); ?></span></p>
                                    <p><span class="phantram_toilet" id="info_toilet-ie"><?php echo $notolet; ?>%</span> <span  class="info-toilet" id="IE8-notoilet-access"><?php echo get_post_meta($post->ID, 'toilet_access_text2', true); ?></span></p>
                                </div>
                            </div>
                            <div class="vietnam large-10 columns end vietnam-ie" id="IE8-﻿vietnam">
                                <div id="IE8-vietnam-div">
                                    <?php echo get_field('content_map_viet_nam'); ?>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div class="scholarship">                        
                            <div class="large-1 columns end ">&nbsp;</div>
                            <div class="large-9 small-20 columns end">
                                <p><?php echo get_post_meta($post->ID, 'bike_text', true); ?></p>
                            </div>
                            <div class="large-8 small-20 columns end">
                                <img src="<?php bloginfo('template_url'); ?>/images/bieudo.png" alt="" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="large-1 columns end ">&nbsp;</div>
            </div>
        </div>
    </div>
<?php endwhile; // end of the loop.  ?>

<script>
    $("#menu-item-234").addClass("current-menu-item");
</script>
<?php get_footer(); ?>
