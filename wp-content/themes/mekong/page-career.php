<?php
/*
  Template Name: Template Career
 */
?>
<?php get_header(); ?>
<?php
if (wpmd_is_phone()) {
    include_once 'mobile/page-career.php';
    get_footer();
    return;
}
?>
<?php while (have_posts()) : the_post(); ?>
    <div class="page-banner">
        <img src="<?php echo get_field('page_banner'); ?>" />
        <div class="row">
            <div class="large-9 small-20 page-decription">
                <?php echo get_field('introduction_page'); ?>
            </div>
        </div>

    </div>
    <div class="main-content">
        <div class="row">
            <div class="large-1 columns end ">&nbsp;</div>
            <div class="large-18 columns end">
                <div class="large-20 columns end ">
                    <div class="title-page-boxmenu" style="margin-top: 70px;">
                        <div class="title-page large-15 left"><?php the_title(); ?></div>
                        <div class="boxmenu large-4 right">
                            <?php the_title(); ?>
                        </div>
                        <div class="icon-boxmenu"></div>
                        <div class="menu-box large-4">
                            <?php
                            $args = array();
                            $args = array(
                                'theme_location' => 'secondary',
                                'container' => FALSE,
                                'menu' => 'Menu about',
                                'menu_class' => '',
                                'echo' => true,
                                'fallback_cb' => '',
                                'depth' => 4,
                                //'after' => '<li class="divider"></li>',                        
                                'menu_id' => '');
                            wp_nav_menu($args);
                            ?>
                        </div>
                    </div>
                </div>
                <div class="page-career">
                    <div class="large-6 small-20 columns end">
                        <?php get_sidebar('career'); ?>
                    </div>
                    <div class="large-1 columns end ">&nbsp;</div>
                    <div class="large-13 small-20 columns end">
                        <?php $page_default = get_field('page_default'); ?>  
                        <?php $content = apply_filters("the_content", $page_default->post_content); ?>
                        <?php echo $content; ?>                        
                    </div>
                </div>
            </div>

            <div class="large-1 columns end ">&nbsp;</div>
        </div>
    </div>
<?php endwhile; // end of the loop.  ?>
<?php get_footer(); ?>

<script>
$("#menu-item-234").addClass("current-menu-item");
</script>