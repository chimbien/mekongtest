<?php
if (wpmd_is_phone()) {
  include_once 'mobile/footer.php';
  get_footer();
  return;
}
?>
<?php if (is_home()): ?>
    <div class="footer" style="position: fixed;bottom: 0;left: 0;z-index: 100;">
    <?php else: ?>
        <div class="footer">
        <?php endif; ?>
        <div class="row">
            <div class="footer-content large-20 columns end">
                <p>&COPY; 2014 MEKONG COMMUNITY DEVELOPMENT CENTER</p>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        $(document).foundation();

    </script>
    <?php //wp_footer(); ?>
</div>

 <!--[if IE 8]>
<link rel="stylesheet" media="screen and (max-width: 1366px)" href="<?php bloginfo('template_url'); ?>/css/IE8-m.css" />
     
<![endif]-->
</body>
</html>